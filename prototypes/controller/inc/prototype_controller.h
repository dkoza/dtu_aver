#pragma once

#include "prototypes/data_interface/inc/pt_data_interface.h"

class RuleEngine;

class PrototypeController
{
	
public:

	std::unique_ptr<PtDataInterface> generateDataInterface(const SiteRegistry & site_registry, 
		const VesselRegistry & vessel_registry, 
		const ContainerGroupRegistry & container_group_registry,
		const ConnectingTimeWindowsRegistry& connecting_time_window_registry,
		const ScenarioParameters & scenario_parameters,
		const RuleEngine & rule_engine,
		const Graph & graph);
	
	void writeDataInterfaceToFile(const PtDataInterface& data_ifc, const std::string& scen_dir) const;

};
