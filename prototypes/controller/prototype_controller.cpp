#include "inc/prototype_controller.h"
#include "rule_engine/inc/rule_engine.h"
#include "prototypes/data_interface/inc/pt_data_interface_builder.h"
#include "prototypes/common/inc/timer.h"
#include "domain/inc/connecting_time_window.h"


std::unique_ptr<PtDataInterface> PrototypeController::generateDataInterface(const SiteRegistry & site_registry, 
	const VesselRegistry& vessel_registry,
	const ContainerGroupRegistry& container_group_registry,
	const ConnectingTimeWindowsRegistry& connecting_time_window_registry,
	const ScenarioParameters& scenario_parameters,
	const RuleEngine & rule_engine,
	const Graph & graph)
{
	BOOST_LOG_TRIVIAL(info) << "[START][PT] Generating data interface";
	
	Timer timer; timer.start();
	auto data_interface = PtDataInterfaceBuilder().buildDataInterface(site_registry, vessel_registry, container_group_registry, connecting_time_window_registry, scenario_parameters, rule_engine, graph);

	BOOST_LOG_TRIVIAL(info) << "[END][PT] Generating data interface (" << timer.getElapsedTime() << " sec.)\n";

	return data_interface;
}


void PrototypeController::writeDataInterfaceToFile(const PtDataInterface& data_ifc, const std::string& scen_dir) const
{
	BOOST_LOG_TRIVIAL(info) << "[START][PT] Writing data interface to files";

	Timer timer; timer.start(); 
	data_ifc.writeDataToFiles(scen_dir);

	BOOST_LOG_TRIVIAL(info) << "[END][PT] Writing data interface to files (" << timer.getElapsedTime() << " sec.)\n";

}
