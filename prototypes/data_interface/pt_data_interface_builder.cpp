#include "inc/pt_data_interface_builder.h"
#include "domain/inc/site.h"
#include "rule_engine/inc/distance_calculator.h"
#include "domain/inc/connecting_time_window.h"


std::unique_ptr<PtDataInterface> PtDataInterfaceBuilder::buildDataInterface(
	const SiteRegistry & site_registry,
	const VesselRegistry& vessel_registry,
	const ContainerGroupRegistry& container_group_registry,
	const ConnectingTimeWindowsRegistry& connecting_time_window_registry,
	const ScenarioParameters& scenario_parameters,
	const RuleEngine & rule_engine,
	const Graph & graph
)
{
	data_interface_ = std::make_unique<PtDataInterface>(graph, rule_engine.getCostCalculator());

	processSites(site_registry);
	processVessels(vessel_registry, graph);
	processVesselSailingPlans();
	processVesselSiteCalls(scenario_parameters, graph);
	processSailingLegs(rule_engine.getDistanceCalculator());
	processContainerGroups(container_group_registry);
	processTransportPlans();
	processConnections(connecting_time_window_registry);	// connections need to be processed after transport plans
	processVertices(graph);	// vertices need to be processed before arcs
	processArcs(graph);	// arcs need to be processed after nodes and after adding vessels and container groups (and related)
	// Availability window data needs to be processed after graph
	processAvailabilityWindows();
	processSiteShifts();

	// Post-processing (requires basic data to be processed)
	matchSailingPlansAndTransportPlans();
	matchVesselSiteCallsAndSiteShifts();
	matchPortStayArcsAndSiteShifts();
	
	identifyControllableSailingLegs();
	identifyControllableTransportPlans();
	identifyControllableConnections();
	identifyInfeasibleTransportPlans();		// needs feasibility of connections to be defined
	//identifyDominatedTransportPlans();	// needs TP feasibility to be defined

	identifyAlwaysFeasibleConnections();

	return std::move(data_interface_);
}


void PtDataInterfaceBuilder::processSites(const SiteRegistry& site_registry)
{
	const auto start = std::chrono::system_clock::now();

	// Add dummy site; it is needed, as not all sites of all vessel_site_calls are represented in the SiteRegistry
	addSiteInterface(nullptr);
	

	for (auto & site_code : site_registry.getSiteCodes())
	{
		const auto & site = site_registry.getSite(site_code);
		addSiteInterface(site);
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->site_ifcs_.size() << " sites (" << diff.count() << " sec.)";

}


void PtDataInterfaceBuilder::processVessels(const VesselRegistry& vessel_registry, const Graph & graph)
{
	const auto start = std::chrono::system_clock::now();

	size_t num_controllable_vsls = 0;
	for (auto vsl_ptr : vessel_registry.getVessels())
	{
		auto & vsl_ifc = addVesselInterface(vsl_ptr);
		vessel_id_to_vessel_int_id_map.insert(std::make_pair(vsl_ptr->getVesselId(), vsl_ifc.getId()));

		auto & source_sink_pair = graph.getSourceSink(vsl_ptr->getVesselId());
		vsl_ifc.source_vtx_id_ = graph.getDigraph().id(source_sink_pair.first);
		vsl_ifc.sink_vtx_id_ = graph.getDigraph().id(source_sink_pair.second);

		num_controllable_vsls += vsl_ifc.isControllable();
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->vessel_ifcs_.size() << " vessels (" << diff.count() << " sec.), of which " << num_controllable_vsls << " are controllable.";
}


void PtDataInterfaceBuilder::processVesselSailingPlans()
{
	const auto start = std::chrono::system_clock::now();

	for (auto & vsl_ifc : data_interface_->vessel_ifcs_)
	{
		// Add sailing plans
		for (const auto & sp : vsl_ifc.vsl_ptr_->getSailingPlans().getNonDatedSailingPlans())
		{
			auto & sp_ifc = addSailingPlanInterface(vsl_ifc.getId(), sp);
			// Add sailing plan ID to sailing_plan_ids_ of vessel
			vsl_ifc.sailing_plan_ids_.push_back(sp_ifc.getId());
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->sailing_plan_ifcs_.size() << " sailing plans (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::processVesselSiteCalls(const ScenarioParameters & scenario_parameters, const Graph& graph)
{
	const auto start = std::chrono::system_clock::now();

	for (auto & sp_ifc : data_interface_->sailing_plan_ifcs_)
	{
		auto & vsl_ifc = data_interface_->vessel_ifcs_[sp_ifc.getVesselId()];

		for (auto & site_call_key : sp_ifc.getSiteCallKeys())
		{
			const auto num_vsl_site_call_ifcs_before = data_interface_->vessel_site_call_ifcs_.size();
			
			// Add vessel site call only if it does not exists
			auto & vessel_site_call_ifc = addVesselSiteCallInterface(vsl_ifc.vsl_ptr_, site_call_key, scenario_parameters, true);

			const bool has_vsl_site_call_been_added = (data_interface_->vessel_site_call_ifcs_.size() > num_vsl_site_call_ifcs_before);

			// Only add VesselSiteCall to site if it did not exist before
			if (has_vsl_site_call_been_added) {
				// Add vessel site call ID to vessel site call IDs of terminal
				data_interface_->site_ifcs_[vessel_site_call_ifc.getSiteId()].vessel_site_call_ids_.push_back(vessel_site_call_ifc.getId());
				data_interface_->vessel_ifcs_[vessel_site_call_ifc.getVesselId()].vessel_site_call_ids_.push_back(vessel_site_call_ifc.getId());
			}

			// Add vessel site call ID to vessel_site_call_ids_ of sailing plan
			sp_ifc.vessel_site_call_ids_.push_back(vessel_site_call_ifc.getId());

			// Add sailing plan ID to sailing_plan_ids_ of vessel site call 
			vessel_site_call_ifc.sailing_plan_ids_.push_back(sp_ifc.getId());
		}
	}

	// Add information about original schedule to vessel site call interfaces
	for (const auto & vsl_ifc : data_interface_->getVesselInterfaces()) {
		for (const auto & sched_port_stay : vsl_ifc.vsl_ptr_->getSailingSchedule().getScheduledPortStays())
		{
			const auto vsc_id = vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.find(vsl_ifc.getName() + sched_port_stay->getKey().asString())->second;
			data_interface_->vessel_site_call_ifcs_[vsc_id].scheduled_port_stay_ = sched_port_stay;
		}
	}
	
	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->vessel_site_call_ifcs_.size() << " vessel site calls (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::processSailingLegs(const DistanceCalculator & distance_calculator)
{
	const auto start = std::chrono::system_clock::now();

	for (auto & sp_ifc : data_interface_->sailing_plan_ifcs_)
	{
		TVesselSiteCallIntId origin_vsc_id = -1;
		for (auto dest_vsc_id : sp_ifc.getVesselSiteCallIds())
		{
			// Add sailing legs if not added yet
			if (dest_vsc_id != sp_ifc.getVesselSiteCallIds().front())
			{
				auto num_sl_ifcs = data_interface_->sailing_leg_ifcs_.size();
				auto & sl_ifc = addSailingLegInterface(sp_ifc.getVesselId(), origin_vsc_id, dest_vsc_id, true);

				// Add this sailing plan ID to leg
				sl_ifc.sailing_plan_ids_.push_back(sp_ifc.getId());

				// Add sailing leg ID to this sailing plan
				sp_ifc.sailing_leg_ids_.push_back(sl_ifc.getId());

				// Calculate distance/pilot data if sailing leg interface did not exist before
				bool sl_ifc_did_exist = num_sl_ifcs == data_interface_->sailing_leg_ifcs_.size();
				if(!sl_ifc_did_exist)
				{
					auto const & site_from_ifc = data_interface_->getSiteInterface(data_interface_->getVesselSiteCallInterface(origin_vsc_id).getSiteId());
					auto const & site_to_ifc = data_interface_->getSiteInterface(data_interface_->getVesselSiteCallInterface(dest_vsc_id).getSiteId());
					
					// Only set distance if both sites are in the data; otherwise, set distance to '0' if one of the sites is a 'dummy' site (ID=0)
					if (!site_from_ifc.isDummySite() && !site_to_ifc.isDummySite()) {
						sl_ifc.distance_ = distance_calculator.calculateDistance(site_from_ifc.site_->getSiteId(), site_to_ifc.site_->getSiteId()).total_distance_;
					}
					else {
						sl_ifc.distance_ = 0.0;
					}
				}
			}

			origin_vsc_id = dest_vsc_id;
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->sailing_leg_ifcs_.size() << " sailing legs (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::processContainerGroups(const ContainerGroupRegistry& cg_registry)
{
	const auto start = std::chrono::system_clock::now();

	for (auto & cg_ptr : cg_registry.getContainerGroups())
	{
		auto & cg_ifc = addContainerGroupInterface(cg_ptr);
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->container_group_ifcs_.size() << " container groups (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::processTransportPlans()
{
	const auto start = std::chrono::system_clock::now();

	for (auto & cg_ifc : data_interface_->container_group_ifcs_)
	{
		// For each transport plan associated with container group:
		for (const auto & tp : cg_ifc.cg_ptr_->getTransportPlans().getTransportPlans())
		{
			auto & tp_ifc = addTransportPlanInterface(cg_ifc.getId(), tp);

			cg_ifc.transport_plan_ids_.push_back(tp_ifc.getId());
			if (tp_ifc.isOriginal()) 
				cg_ifc.original_transport_plan_id_ = tp_ifc.getId();

			// Loop through legs of transport plan
			for (auto & tp_leg : tp->getTransportPlan())
			{
				const auto vessel_site_call_from_id = vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.find(tp_leg.vesselCode + tp_leg.from.asString());
				const auto vessel_site_call_to_id = vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.find(tp_leg.vesselCode + tp_leg.to.asString());

				// Ensure vessel and vessel site calls of this leg exist
				assert(vessel_id_to_vessel_int_id_map.find(tp_leg.vesselId) != vessel_id_to_vessel_int_id_map.end() &&
					vessel_site_call_from_id != vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.end() &&
					vessel_site_call_to_id != vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.end()
				);

				tp_ifc.load_vessel_site_call_ids_.push_back(vessel_site_call_from_id->second);
				tp_ifc.discharge_vessel_site_call_ids_.push_back(vessel_site_call_to_id->second);
				tp_ifc.compatible_sailing_plans_per_transport_leg_.emplace_back(std::vector<TSailingPlanIntId>());

				data_interface_->vessel_site_call_ifcs_[vessel_site_call_from_id->second].transport_plan_load_ids_.push_back(tp_ifc.getId());
				data_interface_->vessel_site_call_ifcs_[vessel_site_call_to_id->second].transport_plan_discharge_ids_.push_back(tp_ifc.getId());
			}

			// Add transport plan ID to destination vessel site call interface
			data_interface_->vessel_site_call_ifcs_[tp_ifc.getDestinationVesselSiteCallId()].transport_plan_destination_ids_.push_back(tp_ifc.getId());
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->transport_plan_ifcs_.size() << " transport plans (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::processConnections(const ConnectingTimeWindowsRegistry& connecting_time_window_registry)
{
	const auto start = std::chrono::system_clock::now();

	for (auto & tp_ifc : data_interface_->transport_plan_ifcs_)
	{
		// For each transport plan associated with container group:
		for (size_t connection_num = 0; connection_num < tp_ifc.getNumTransshipments(); connection_num++)
		{
			const auto vsc_from_id = tp_ifc.getDischargeVesselSiteCallIds()[connection_num];
			const auto vsc_to_id = tp_ifc.getLoadVesselSiteCallIds()[connection_num + 1];

			auto & cnx_ifc = addConnectionInterface(connecting_time_window_registry, vsc_from_id, vsc_to_id, true);

			cnx_ifc.transport_plan_ids_.push_back(tp_ifc.getId());
			tp_ifc.connection_ids_.push_back(cnx_ifc.getId());
		}
		
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->connection_ifcs_.size() << " transshipment connections (" << diff.count() << " sec.)";
}


void PtDataInterfaceBuilder::processAvailabilityWindows()
{
	const auto start = std::chrono::system_clock::now();

	for (auto & site_ifc : data_interface_->site_ifcs_)
	{
		if (site_ifc.site_ == nullptr)	// If site is dummy site
		{
			continue;
		}

		for(auto & aw : site_ifc.site_->getAvailabilityWindows())
		{
			auto & aw_ifc = addAvailabilityWindowInterface(site_ifc.getId(), aw);

			if (aw.getRestrictedToVessel().is_initialized()) {
				aw_ifc.limited_to_vessel_ids_.push_back(vessel_id_to_vessel_int_id_map[aw.getRestrictedToVessel().value()]);
			}
			aw_ifc.is_limited_to_fuel_group_ = aw.getRestrictedToVesselGroups().is_initialized();
			site_ifc.availability_window_ids_.push_back(aw_ifc.getId());

			const auto & limited_to_vsl_ids = aw_ifc.getLimitedToVesselIds();

			for(auto vsc_id : site_ifc.getVesselSiteCallIds())
			{
				auto & vsc_ifc = data_interface_->vessel_site_call_ifcs_[vsc_id];
				
				// If availability window is limited to particular vessel IDs, skip vessel site call if availability window does not apply to it
				if (!limited_to_vsl_ids.empty() && std::find(limited_to_vsl_ids.begin(), limited_to_vsl_ids.end(), vsc_ifc.getVesselId()) == limited_to_vsl_ids.end())
				{
					continue;
				}

				const auto & vsl_ifc = data_interface_->getVesselInterface(vsc_ifc.getVesselId());
				if (vsl_ifc.isControllable() && vsc_ifc.getTimeSpan().intersects(aw_ifc.getTimeSpan())) {
					aw_ifc.vessel_site_call_ids_.push_back(vsc_id);
					vsc_ifc.availability_window_ids_.push_back(aw_ifc.getId());
				}
			}
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->availability_window_ifcs_.size() << " availability windows (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::processSiteShifts()
{
	const auto start = std::chrono::system_clock::now();

	for (auto & aw_ifc : data_interface_->availability_window_ifcs_)
	{
		// Shifts are defined per site, but we only add those that are part of at least one availability window
		for(auto & aw_shift : aw_ifc.availability_window_.getShifts())
		{
			// only add site shift if not existent yet
			auto & site_shift_ifc = addSiteShiftInterface(aw_ifc.getSiteId(), aw_shift, true);

			site_shift_ifc.availability_window_ids_.push_back(aw_ifc.getId());
			aw_ifc.shift_ids_.push_back(site_shift_ifc.getId());
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->site_shift_ifcs_.size() << " site shifts (" << diff.count() << " sec.)";
}


void PtDataInterfaceBuilder::processVertices(const Graph& graph)
{
	const auto start = std::chrono::system_clock::now();

	auto & graph_ifc = data_interface_->graph_ifc_;
	const auto & dg = graph.getDigraph();

	for (lemon::SmartDigraph::NodeIt v(dg); v != lemon::INVALID; ++v)
	{
		const auto vtx_iid = dg.id(v);
		const auto vtx_type = graph.getNodeType(v);

		if (vtx_type == Graph::kNODE_TYPE::kVESSEL_ARR || vtx_type == Graph::kNODE_TYPE::kVESSEL_DEP)
		{
			const auto scr = graph.getSiteCallRecord(v);

			TSiteCallKey sck(scr->getSiteCode(), scr->getArrivalVoyageCode(), scr->getDepartureVoyageCode(), scr->getServiceCode());

			const auto vsc_id = vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.find(scr->getVessel()->getVesselCode() + sck.asString())->second;

			graph_ifc.vessel_vtx_ids_.push_back(vtx_iid);
			graph_ifc.vtx_type_ids_from_vtx_int_id_[vtx_iid] = graph_ifc.vessel_vtx_ids_.size() - 1;
			graph_ifc.vsl_site_call_id_of_vessel_nodes_.push_back(vsc_id);
		}
		/*
		else if (vtx_type == Graph::kNODE_TYPE::kCARGO_ORIG)
		{
			// We need to get any outgoing arc, as Graph class does only allow to get ContainerGroup from entry arcs, not from the node
			for (lemon::SmartDigraph::OutArcIt a(dg, v); a != lemon::INVALID; ++a) {
				const auto cg_iid = cg_shipment_num_to_cg_int_id_map[graph.getContainerGroup(a)->getShipmentNumber()];
				auto & cg_ifc = data_interface_->container_group_ifcs_[cg_iid];
				cg_ifc.origin_vtx_id_ = dg.id(v);
				break;
			}
		}
		else if (vtx_type == Graph::kNODE_TYPE::kCARGO_DEST)
		{
			// We need to get any outgoing arc, as Graph class does only allow to get ContainerGroup from entry arcs, not from the node
			for (lemon::SmartDigraph::InArcIt a(dg, v); a != lemon::INVALID; ++a) {
				const auto cg_iid = cg_shipment_num_to_cg_int_id_map[graph.getContainerGroup(a)->getShipmentNumber()];
				auto & cg_ifc = data_interface_->container_group_ifcs_[cg_iid];
				cg_ifc.destination_vtx_id_ = dg.id(v);
				break;
			}
		}
		*/
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Processed " << dg.nodeNum() << " nodes (" << diff.count() << " sec.)";
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->graph_ifc_.vessel_vtx_ids_.size() << " vessel nodes.";
	//BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->graph_ifc_.transshipment_arc_ids_.size() << " of " << ttl_num_ts_arcs << " transshipment arcs.";
	//BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->graph_ifc_.port_stay_arc_ids_.size() << " port stay arcs.";
}


void PtDataInterfaceBuilder::processArcs(const Graph& graph)
{
	const auto start = std::chrono::system_clock::now();

	const auto & dg = graph.getDigraph();

	size_t ttl_num_ts_arcs = 0;
	for(lemon::SmartDigraph::ArcIt a(dg); a!=lemon::INVALID; ++a)
	{
		const auto arc_id = dg.id(a);
		const auto arc_type = graph.getArcType(a);
		
		if (arc_type == Graph::kARC_TYPE::kSAILING)
		{
			addSailingArc(arc_id);
		}
		else if(arc_type == Graph::kARC_TYPE::kPORT_STAY)
		{
			addPortStayArc(arc_id);
		}
		else if (arc_type == Graph::kARC_TYPE::kTRANSSHIPMENT)
		{
			ttl_num_ts_arcs++;
			// Note: Transshipment arcs are only added if relevant for model (i.e. if corresponding connection is used by at least one transport plan)
			addTransshipmentArc(arc_id);
		}
		/*
		else if (arc_type == Graph::kARC_TYPE::kCARGO_ENTRY)
		{
			addCargoEntryArc(arc_id);
		}
		else if (arc_type == Graph::kARC_TYPE::kCARGO_EXIT)
		{
			addCargoExitArc(arc_id);
		}
		*/
		else
		{

		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Processed " << dg.arcNum() << " arcs (" << diff.count() << " sec.)";
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->graph_ifc_.sailing_arc_ids_.size() << " sailing arcs.";
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->graph_ifc_.transshipment_arc_ids_.size() << " of " << ttl_num_ts_arcs << " transshipment arcs.";
	BOOST_LOG_TRIVIAL(info) << "  Added " << data_interface_->graph_ifc_.port_stay_arc_ids_.size() << " port stay arcs.";
}


void PtDataInterfaceBuilder::matchSailingPlansAndTransportPlans()
{
	const auto start = std::chrono::system_clock::now();

	for(auto & tp_ifc : data_interface_->transport_plan_ifcs_)
	{
		for(size_t tp_leg_num=0; tp_leg_num < tp_ifc.getNumLegs(); tp_leg_num++)
		{
			auto load_vsc_iid = tp_ifc.getLoadVesselSiteCallIds()[tp_leg_num];
			auto disch_vsc_iid = tp_ifc.getDischargeVesselSiteCallIds()[tp_leg_num];
			
			assert(data_interface_->vessel_site_call_ifcs_[load_vsc_iid].getVesselId() == data_interface_->vessel_site_call_ifcs_[disch_vsc_iid].getVesselId());
			const auto vsl_iid = data_interface_->vessel_site_call_ifcs_[load_vsc_iid].getVesselId();

			for(auto sp_iid : data_interface_->vessel_ifcs_[vsl_iid].getSailingPlanIds())
			{
				auto & sp_ifc = data_interface_->sailing_plan_ifcs_[sp_iid];
				const auto & vsc_iids = sp_ifc.getVesselSiteCallIds();
				if(std::find(vsc_iids.begin(), vsc_iids.end(), load_vsc_iid) != vsc_iids.end()
					&& std::find(vsc_iids.begin(), vsc_iids.end(), disch_vsc_iid) != vsc_iids.end()
				)
				{
					tp_ifc.compatible_sailing_plans_per_transport_leg_[tp_leg_num].push_back(sp_iid);
					sp_ifc.compatible_tp_id_leg_num_pairs_.emplace_back(tp_ifc.getId(), tp_leg_num);
				}
			}
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Matched " << data_interface_->transport_plan_ifcs_.size() << " transport plans with " << data_interface_->sailing_plan_ifcs_.size() << " sailing plans (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::matchVesselSiteCallsAndSiteShifts()
{	
	const auto start = std::chrono::system_clock::now();

	size_t num_controllable_vsc = 0;

	for (auto & vsc_ifc : data_interface_->vessel_site_call_ifcs_)
	{
		if (!data_interface_->getVesselInterface(vsc_ifc.getVesselId()).isControllable())
			continue;

		num_controllable_vsc++;
		std::set<size_t> site_shift_ids;

		for (auto aw_iid : vsc_ifc.getAvailabilityWindowIds()) {
			for (auto site_shift_iid : data_interface_->getAvailabilityWindowInterface(aw_iid).getSiteShiftIds()) {
				const auto & site_shift_ifc = data_interface_->getSiteShiftInterface(site_shift_iid);

				if (vsc_ifc.getTimeSpan().intersects(site_shift_ifc.getTimeSpan())) {
					site_shift_ids.insert(site_shift_iid);
				}
			}
		}

		// Get unique site_shift_ids
		for(auto site_shift_iid : site_shift_ids)
		{
			auto & site_shift_ifc = data_interface_->site_shift_ifcs_[site_shift_iid];
			
			site_shift_ifc.vessel_site_call_ids_.push_back(vsc_ifc.getId());
			vsc_ifc.site_shift_ids_.push_back(site_shift_ifc.getId());
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Matched " << num_controllable_vsc << " vessel site calls with site shifts (" << diff.count() << " sec.)";
}

/*
 * Determines for each port stay arc of controllable vessels which site shifts they 'consume' if used,
 * and, for each site shift, which port stay arcs of controllable vessels may consume the site shift.
 */
void PtDataInterfaceBuilder::matchPortStayArcsAndSiteShifts()
{
	const auto start = std::chrono::system_clock::now();

	auto & graph_ifc = data_interface_->graph_ifc_;
	for(TArcIntId arc_iid : graph_ifc.getPortStayArcIds())
	{
		auto arc_time_span = graph_ifc.getArcTimeSpan(arc_iid);

		const auto & vsc_ifc = graph_ifc.getVesselSiteCallInterfaceFromPortStayArc(arc_iid);

		// Skip site calls of non-controllable vessels
		if (!data_interface_->getVesselInterface(vsc_ifc.getVesselId()).isControllable())
			continue;

		for (auto & site_shift_iid : vsc_ifc.getSiteShiftIds())
		{
			auto & shift_ifc = data_interface_->site_shift_ifcs_[site_shift_iid];

			if (arc_time_span.intersects(shift_ifc.getTimeSpan())) {
				shift_ifc.port_stay_arc_ids_.push_back(arc_iid);
				graph_ifc.site_shift_ids_of_port_stay_arcs_[graph_ifc.arc_type_ids_from_arc_int_id_[arc_iid]].push_back(site_shift_iid);
			}
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Matched " << graph_ifc.getPortStayArcIds().size() << " port stay arcs with site shifts (" << diff.count() << " sec.)";
}

void PtDataInterfaceBuilder::identifyControllableSailingLegs()
{
	const auto start = std::chrono::system_clock::now();

	for (auto & sl_ifc : data_interface_->sailing_leg_ifcs_)
	{
		if (data_interface_->getVesselSiteCallInterface(sl_ifc.getOriginVesselSiteCallId()).isControllable()
			|| data_interface_->getVesselSiteCallInterface(sl_ifc.getDestinationVesselSiteCallId()).isControllable())
		{
			sl_ifc.can_control_ = true;
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Checked " << data_interface_->transport_plan_ifcs_.size() << " transport plans whether controllable or not (" << diff.count() << " sec.)";
}

/*
 * Checks for each transport plan, if any of the load/discharge vessel site calls are controllable and
 * if so, marks the corresponding transport plan as controllable as well
 */
void PtDataInterfaceBuilder::identifyControllableTransportPlans()
{
	const auto start = std::chrono::system_clock::now();
	
	for (auto & tp_ifc : data_interface_->transport_plan_ifcs_)
	{
		if(data_interface_->getVesselSiteCallInterface(tp_ifc.getDestinationVesselSiteCallId()).isControllable())
		{
			tp_ifc.can_control_ = true;
			continue;
		}
		for(auto vsc_iid : tp_ifc.getLoadVesselSiteCallIds())
		{
			if(data_interface_->getVesselSiteCallInterface(vsc_iid).isControllable())
			{
				tp_ifc.can_control_ = true;
				break;
			}
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Checked " << data_interface_->transport_plan_ifcs_.size() << " transport plans whether controllable or not (" << diff.count() << " sec.)";
}

/*
 * Checks for each transport plan, if any of the load/discharge vessel site calls are controllable and
 * if so, marks the corresponding transport plan as controllable as well
 */
void PtDataInterfaceBuilder::identifyControllableConnections()
{
	const auto start = std::chrono::system_clock::now();

	for (auto & cnx_ifc : data_interface_->connection_ifcs_)
	{
		if (data_interface_->getVesselSiteCallInterface(cnx_ifc.getVesselSiteCallFromId()).isControllable()
			|| data_interface_->getVesselSiteCallInterface(cnx_ifc.getVesselSiteCallToId()).isControllable())
		{
			cnx_ifc.can_control_ = true;
			continue;
		}
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Checked " << data_interface_->connection_ifcs_.size() << " connections whether controllable or not (" << diff.count() << " sec.)";
}


/*
 * Identifies infeasible and dominated transport plans.
 * A transport plan is infeasible, iff any of the transshipment connections used is infeasible
 */
void PtDataInterfaceBuilder::identifyInfeasibleTransportPlans()
{
	const auto start = std::chrono::system_clock::now();

	for(const auto & cnx_ifc : data_interface_->getConnectionInterfaces())
	{
		if(!cnx_ifc.isFeasible())
		{
			for(auto tp_id : cnx_ifc.getTransportPlanIds())
			{
				data_interface_->transport_plan_ifcs_[tp_id].is_feasible_ = false;
				//BOOST_LOG_TRIVIAL(info) << "Transport plan " << tp_id << " is infeasible!";
			}
		}
	}

	/*
	for (const auto & cg_ifc : data_interface_->getContainerGroupInterfaces())
	{
		for (auto & tp_id : cg_ifc.getTransportPlanIds())
		{
			auto & tp_ifc = data_interface_->transport_plan_ifcs_[tp_id];

			// Check and define TP feasibility (by default, is_feasible_=true)
			for (auto cnx_id : tp_ifc.getConnectionIds())
			{
				if (!data_interface_->getConnectionInterface(cnx_id).isFeasible())
				{
					tp_ifc.is_feasible_ = false;
					break;
				}
			}
		}
	}
	*/
	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Checked " << data_interface_->transport_plan_ifcs_.size() << " transport plans whether feasible or not (" << diff.count() << " sec.)";
}

/*
 * Identifies dominated transport plans. 
 * A transport plan is dominated, iff
 * (1) it is non-controllable
 * (2) a cheaper, non-controllable transport plan exists
 * Note: This dominance rule is only valid under an infinite-vessel-capacity assumption
 */
void PtDataInterfaceBuilder::identifyDominatedTransportPlans()
{
	const auto start = std::chrono::system_clock::now();

	for(const auto & cg_ifc : data_interface_->getContainerGroupInterfaces())
	{
		if (cg_ifc.getTransportPlanIds().empty())
			continue;
		
		size_t dominant_tp_id = 0;
		double dominant_cost = std::numeric_limits<double>::max();
		
		for(auto & tp_id : cg_ifc.getTransportPlanIds())
		{
			auto & tp_ifc = data_interface_->transport_plan_ifcs_[tp_id];

			// First, check and define TP feasibility
			for(auto cnx_id : tp_ifc.getConnectionIds())
			{
				if(!data_interface_->getConnectionInterface(cnx_id).isFeasible())
				{
					tp_ifc.is_feasible_ = false;
					break;
				}
			}

			// Controllable TPs are never dominated
			if (tp_ifc.isControllable()) {
				tp_ifc.is_dominated_ = false;
			}
			else if (!tp_ifc.isFeasible())
			{
			// Infeasible TPs are always dominated
				tp_ifc.is_dominated_ = true;
			}
			else
			// By default, first mark all non-controllable transport plans as dominated
			// only select the cheapest as non-dominated
			{
				tp_ifc.is_dominated_ = true;
				const auto & dest_vsc_ifc = data_interface_->getVesselSiteCallInterface(tp_ifc.getDestinationVesselSiteCallId());
				const double cost = data_interface_->getTransportPlanUnitHandlingCost(tp_ifc) * cg_ifc.getNumTEU()
					+ data_interface_->getContainerGroupUnitDelayPenalty(cg_ifc, dest_vsc_ifc.getPortStayArcIds().front()) * cg_ifc.getNumTEU();

				if (cost < dominant_cost)
				{
					dominant_cost = cost;
					dominant_tp_id = tp_ifc.getId();
				}
			}
		}
		// Mark only the lowest-cost TP of all non-controllable TPs as non-dominated
		data_interface_->transport_plan_ifcs_[dominant_tp_id].is_dominated_ = false;
	}

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Checked " << data_interface_->transport_plan_ifcs_.size() << " transport plans whether dominated or not (" << diff.count() << " sec.)";
}


void PtDataInterfaceBuilder::identifyAlwaysFeasibleConnections()
{
	const auto start = std::chrono::system_clock::now();
	
	for(auto & cnx_ifc : data_interface_->connection_ifcs_)
	{
		// Non-controllable connections are not relevant
		if (!cnx_ifc.isControllable())
			continue;

		const auto & vsc_from_ifc = data_interface_->getVesselSiteCallInterface(cnx_ifc.getVesselSiteCallFromId());
		const auto & vsc_to_ifc = data_interface_->getVesselSiteCallInterface(cnx_ifc.getVesselSiteCallToId());
		
		const auto & latest_unloading_time = cnx_ifc.getCtwStartType() == eSITE_CALL_TYPE::eARR ? vsc_from_ifc.getTimeSpanArr().end() : vsc_from_ifc.getTimeSpanDep().end();
		const auto & earliest_loading_time = cnx_ifc.getCtwEndType() == eSITE_CALL_TYPE::eARR ? vsc_to_ifc.getTimeSpanArr().begin() : vsc_to_ifc.getTimeSpanDep().begin();

		if(earliest_loading_time - latest_unloading_time > cnx_ifc.getMinimumConnectingTime())
		{
			cnx_ifc.is_always_feasible_ = true;
			//BOOST_LOG_TRIVIAL(info) << cnx_ifc.getId() << ": latest_unload_time=" << latest_unloading_time << ", earliest_load_time=" << earliest_loading_time << ", diff=" << earliest_loading_time - latest_unloading_time << ", minCTW=" << cnx_ifc.getMinimumConnectingTime() << ", n_arcs=" << cnx_ifc.getTransshipmentArcIds().size();
		}
	}
	//BOOST_LOG_TRIVIAL(info) << "Found " << num_always_feas_cnx << " always feasible connections using " << arcs << " transshipment arcs!";

	std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
	BOOST_LOG_TRIVIAL(info) << "  Checked " << data_interface_->connection_ifcs_.size() << " connections whether always feasible or not (" << diff.count() << " sec.)";

}


PtVesselInterface & PtDataInterfaceBuilder::addVesselInterface(const TConstVesselPtr & vsl_ptr)
{
	auto id = data_interface_->vessel_ifcs_.size();
	data_interface_->vessel_ifcs_.emplace_back(id, vsl_ptr);

	return data_interface_->vessel_ifcs_.back();
}

PtVesselSiteCallInterface & PtDataInterfaceBuilder::addVesselSiteCallInterface(const TConstVesselPtr & vsl_ptr, const TSiteCallKey & site_call_key, const ScenarioParameters & scenario_parameters, bool check_if_exists)
{
	// Check if vessel site call exists, and if so, return reference to existing vessel site call
	if (check_if_exists) {
		auto result = vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.find(vsl_ptr->getVesselCode() + site_call_key.asString());
		// If exists
		if (result != vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.end())
			return data_interface_->vessel_site_call_ifcs_[result->second];
	}

	auto id = data_interface_->vessel_site_call_ifcs_.size();
	auto vessel_int_id = vessel_id_to_vessel_int_id_map[vsl_ptr->getVesselId()];

	// If site is not in SiteRegistry, use site_id = 0 for dummy site
	const auto result = site_code_to_site_int_id.find(site_call_key.getSiteCode());
	auto site_id = (result != site_code_to_site_int_id.end() ? result->second : 0);

	const bool is_omission_allowed = (vsl_ptr->isScenarioVessel()
		&& scenario_parameters.getRecoveryOptions(vsl_ptr->getVesselId()).getSiteCallOptions(site_call_key).isOmissionAllowed());

	data_interface_->vessel_site_call_ifcs_.emplace_back(id, vessel_int_id, site_id, vsl_ptr, site_call_key, is_omission_allowed);

	// Add vessel site call ID to map
	vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map.insert(std::make_pair(vsl_ptr->getVesselCode() + site_call_key.asString(), id));

	return data_interface_->vessel_site_call_ifcs_.back();
}

PtSiteInterface & PtDataInterfaceBuilder::addSiteInterface(const Site * site, bool check_if_exists)
{
	// Check if terminal exists, and if so, return reference to existing terminal
	if (check_if_exists) {
		auto result = site_code_to_site_int_id.find(site->getSiteCode());
		// If exists
		if (result != site_code_to_site_int_id.end())
			return data_interface_->site_ifcs_[result->second];
	}

	auto id = data_interface_->site_ifcs_.size();
	data_interface_->site_ifcs_.emplace_back(id, site);
	auto & site_ifc = data_interface_->site_ifcs_.back();

	site_code_to_site_int_id.insert(std::make_pair(site_ifc.getSiteCode(), site_ifc.getId()));
	site_id_shift_time_span_to_site_shift_int_id_map.emplace_back();	// Prepare vector of mappings used to map site_shifts to TSiteShiftIntIds

	return site_ifc;
}

PtSailingPlanInterface & PtDataInterfaceBuilder::addSailingPlanInterface(TVesselIntId vessel_id, const TVesselNonDatedSailingPlan & sp)
{

	auto id = data_interface_->sailing_plan_ifcs_.size();
	data_interface_->sailing_plan_ifcs_.emplace_back(id, vessel_id, sp);

	return data_interface_->sailing_plan_ifcs_.back();
}

PtContainerGroupInterface & PtDataInterfaceBuilder::addContainerGroupInterface(const TContainerGroupPtr cg_ptr)
{
	auto id = data_interface_->container_group_ifcs_.size();
	data_interface_->container_group_ifcs_.emplace_back(id, cg_ptr);
	auto & cg_ifc = data_interface_->container_group_ifcs_.back();
	
	cg_shipment_num_to_cg_int_id_map.insert(std::make_pair(cg_ptr->getShipmentNumber(), cg_ifc.getId()));

	return cg_ifc;
}

PtTransportPlanInterface & PtDataInterfaceBuilder::addTransportPlanInterface(TContainerGroupIntId container_group_id, const TContainerGroupTransportPlanConstPtr & tp)
{
	auto id = data_interface_->transport_plan_ifcs_.size();
	data_interface_->transport_plan_ifcs_.emplace_back(id, container_group_id, tp);

	return data_interface_->transport_plan_ifcs_.back();
}

PtSailingLegInterface & PtDataInterfaceBuilder::addSailingLegInterface(TVesselIntId vessel_id, 
	TVesselSiteCallIntId origin_vessel_site_call_id,
	TVesselSiteCallIntId destination_vessel_site_call_id,
	bool check_if_exists
)
{
	// Check if sailing leg exists, and if so, return reference to existing sailing leg
	if (check_if_exists) {
		const auto result = vessel_site_call_from_to_pair_to_leg_int_id_map.find(std::make_pair(origin_vessel_site_call_id, destination_vessel_site_call_id));
		// If exists
		if (result != vessel_site_call_from_to_pair_to_leg_int_id_map.end())
			return data_interface_->sailing_leg_ifcs_[result->second];
	}

	auto id = data_interface_->sailing_leg_ifcs_.size();
	data_interface_->sailing_leg_ifcs_.emplace_back(id, vessel_id, origin_vessel_site_call_id, destination_vessel_site_call_id);
	auto & sailing_leg_ifc = data_interface_->sailing_leg_ifcs_.back();

	vessel_site_call_from_to_pair_to_leg_int_id_map.insert(std::make_pair(std::make_pair(origin_vessel_site_call_id, destination_vessel_site_call_id), sailing_leg_ifc.getId()));

	return sailing_leg_ifc;
}

PtConnectionInterface& PtDataInterfaceBuilder::addConnectionInterface(const ConnectingTimeWindowsRegistry& connecting_time_window_registry, TVesselSiteCallIntId vessel_site_call_from_id, TVesselSiteCallIntId vessel_site_call_to_id, bool check_if_exists)
{
	// Check if sailing leg exists, and if so, return reference to existing sailing leg
	if (check_if_exists) {
		const auto result = vessel_site_call_from_to_pair_to_connection_int_id_map.find(std::make_pair(vessel_site_call_from_id, vessel_site_call_to_id));
		// If exists
		if (result != vessel_site_call_from_to_pair_to_connection_int_id_map.end())
			return data_interface_->connection_ifcs_[result->second];
	}

	auto id = data_interface_->connection_ifcs_.size();

	const auto & vsc_from_ifc = data_interface_->getVesselSiteCallInterface(vessel_site_call_from_id);
	const auto & vsc_to_ifc = data_interface_->getVesselSiteCallInterface(vessel_site_call_to_id);
	const auto & ctw = connecting_time_window_registry.findConnectingTimeWindow(vsc_from_ifc.site_call_key_, vsc_to_ifc.site_call_key_);
	
	data_interface_->connection_ifcs_.emplace_back(id, ctw, vessel_site_call_from_id, vessel_site_call_to_id);
	auto & connection_ifc = data_interface_->connection_ifcs_.back();

	vessel_site_call_from_to_pair_to_connection_int_id_map.insert(std::make_pair(std::make_pair(vessel_site_call_from_id, vessel_site_call_to_id), connection_ifc.getId()));

	return connection_ifc;
}

PtAvailabilityWindowInterface& PtDataInterfaceBuilder::addAvailabilityWindowInterface(
	TSiteIntId site_id, const SiteAvailabilityWindow& availability_window)
{
	auto id = data_interface_->availability_window_ifcs_.size();
	data_interface_->availability_window_ifcs_.emplace_back(id, site_id, availability_window);

	return data_interface_->availability_window_ifcs_.back();
}

PtSiteShiftInterface& PtDataInterfaceBuilder::addSiteShiftInterface(TSiteIntId site_id,
	const SiteShift& shift, bool check_if_exists)
{
	// Check site shift exists, and if so, return reference to existing site shift
	if (check_if_exists) {
		const auto result = site_id_shift_time_span_to_site_shift_int_id_map[site_id].find(shift.getShift());
		// If exists
		if (result != site_id_shift_time_span_to_site_shift_int_id_map[site_id].end())
			return data_interface_->site_shift_ifcs_[result->second];
	}

	auto id = data_interface_->site_shift_ifcs_.size();
	data_interface_->site_shift_ifcs_.emplace_back(id, site_id, shift);
	auto & site_shift_ifc = data_interface_->site_shift_ifcs_.back();

	site_id_shift_time_span_to_site_shift_int_id_map[site_id].insert(std::make_pair(shift.getShift(), site_shift_ifc.getId()));

	return site_shift_ifc;
}

void PtDataInterfaceBuilder::addSailingArc(TArcIntId arc_iid)
{
	auto & graph_ifc = data_interface_->graph_ifc_;
	
	const auto & vsc_from_ifc = graph_ifc.getVesselSiteCallInterfaceFromVertex(graph_ifc.getSourceNodeId(arc_iid));
	const auto & vsc_to_ifc = graph_ifc.getVesselSiteCallInterfaceFromVertex(graph_ifc.getSinkNodeId(arc_iid));

	graph_ifc.sailing_arc_ids_.push_back(arc_iid);
	graph_ifc.arc_type_ids_from_arc_int_id_[arc_iid] = graph_ifc.sailing_arc_ids_.size() - 1;
	graph_ifc.vsl_site_call_from_id_of_sailing_arcs_.push_back(vsc_from_ifc.getId());
	graph_ifc.vsl_site_call_to_id_of_sailing_arcs_.push_back(vsc_to_ifc.getId());
	
	const auto leg_id = vessel_site_call_from_to_pair_to_leg_int_id_map.find(std::make_pair(vsc_from_ifc.getId(), vsc_to_ifc.getId()))->second;
	data_interface_->sailing_leg_ifcs_[leg_id].arc_ids_.push_back(arc_iid);
	graph_ifc.sailing_leg_id_of_sailing_arcs_.push_back(leg_id);
}

void PtDataInterfaceBuilder::addPortStayArc(TArcIntId arc_iid)
{
	auto & graph_ifc = data_interface_->graph_ifc_;
	
	const auto vtx_from_id = graph_ifc.getSourceNodeId(arc_iid);
	const auto vtx_to_id = graph_ifc.getSinkNodeId(arc_iid);
	const auto vsc_id = graph_ifc.getVesselSiteCallInterfaceFromVertex(graph_ifc.getSourceNodeId(arc_iid)).getId();
	auto & vsc_ifc = data_interface_->vessel_site_call_ifcs_[vsc_id];

	vsc_ifc.port_stay_arc_ids_.push_back(arc_iid);

	graph_ifc.port_stay_arc_ids_.push_back(arc_iid);
	graph_ifc.arc_type_ids_from_arc_int_id_[arc_iid] = graph_ifc.port_stay_arc_ids_.size() - 1;
	graph_ifc.vsl_site_call_id_of_port_stay_arcs_.push_back(vsc_ifc.getId());
	graph_ifc.site_shift_ids_of_port_stay_arcs_.emplace_back();	// add empty vector

	// Update PtVesselSiteCallInterface time_span_arr_ and time_span_dep_ based on arc
	vsc_ifc.time_span_arr_ = TTimePeriod(std::min(vsc_ifc.time_span_arr_.begin(), graph_ifc.getVertexTime(vtx_from_id)), std::max(vsc_ifc.time_span_arr_.end(), graph_ifc.getVertexTime(vtx_from_id)));
	vsc_ifc.time_span_dep_ = TTimePeriod(std::min(vsc_ifc.time_span_dep_.begin(), graph_ifc.getVertexTime(vtx_to_id)), std::max(vsc_ifc.time_span_dep_.end(), graph_ifc.getVertexTime(vtx_to_id)));

	/* // Below is for debugging purposes only. Code relies on scr_from and scr_to of a port stay arc to be identical.
	const auto scr_from = graph.getSiteCallRecord(dg.source(a));
	const auto scr_to = graph.getSiteCallRecord(dg.target(a));
	if (scr_from->getSiteCode() != (scr_to->getSiteCode())
		|| scr_from->getArrVoyage() != scr_to->getArrVoyage()
		|| scr_from->getDepVoyage() != scr_to->getDepVoyage()
		|| scr_from->getServiceCode() != scr_to->getServiceCode())
		BOOST_LOG_TRIVIAL(warning) << "Port stay arc " << dg.id(a) << ": scr_from != scr_to. scr_from = " << scr_from->toString() << ", scr_to = " << scr_from->toString();
	*/
}

void PtDataInterfaceBuilder::addTransshipmentArc(TArcIntId arc_iid)
{
	auto & graph_ifc = data_interface_->graph_ifc_;
	
	const auto & vsc_from_ifc = graph_ifc.getVesselSiteCallInterfaceFromVertex(graph_ifc.getSourceNodeId(arc_iid));
	const auto & vsc_to_ifc = graph_ifc.getVesselSiteCallInterfaceFromVertex(graph_ifc.getSinkNodeId(arc_iid));

	auto result = vessel_site_call_from_to_pair_to_connection_int_id_map.find(std::make_pair(vsc_from_ifc.getId(), vsc_to_ifc.getId()));
	if (result != vessel_site_call_from_to_pair_to_connection_int_id_map.end()) {
		// Only add transshipment arc to PtGraphInterface if the corresponding connection is used by at least one transport plan
		auto cnx_id = result->second;
		data_interface_->connection_ifcs_[cnx_id].arc_ids_.push_back(arc_iid);
		
		graph_ifc.transshipment_arc_ids_.push_back(arc_iid);
		graph_ifc.arc_type_ids_from_arc_int_id_[arc_iid] = graph_ifc.transshipment_arc_ids_.size() - 1;
		graph_ifc.connection_id_of_transshipment_arcs_.push_back(cnx_id);
	}
	else
	{
		//BOOST_LOG_TRIVIAL(warning) << "  No connection found for VesselSiteCall pair (" << vsc_id_from << ", " << vsc_id_to << "): " << data_interface_->vessel_site_call_ifcs_[vsc_id_from].getName() << " => " << data_interface_->vessel_site_call_ifcs_[vsc_id_to].getName();
	}
}
/*
void PtDataInterfaceBuilder::addCargoEntryArc(TArcIntId arc_iid)
{
	auto & graph_ifc = data_interface_->graph_ifc_;
	const auto & graph = graph_ifc.graph_;
	const auto & dg = graph.getDigraph();
	const auto & a = dg.arcFromId(arc_iid);

	const auto cg_ptr = graph.getContainerGroup(a);

	const auto cg_iid = cg_shipment_num_to_cg_int_id_map.find(cg_ptr->getShipmentNumber())->second;
	auto & cg_ifc = data_interface_->container_group_ifcs_[cg_iid];
	//cg_ifc.cargo_entry_arc_id = arc_iid;

	graph_ifc.cargo_entry_arc_ids_.push_back(arc_iid);
	graph_ifc.arc_type_ids_from_arc_int_id_[arc_iid] = graph_ifc.cargo_entry_arc_ids_.size() - 1;
	graph_ifc.cg_id_of_cargo_entry_arcs_.push_back(cg_iid);
}

void PtDataInterfaceBuilder::addCargoExitArc(TArcIntId arc_iid)
{
	auto & graph_ifc = data_interface_->graph_ifc_;
	const auto & graph = graph_ifc.graph_;
	const auto & dg = graph.getDigraph();
	const auto & a = dg.arcFromId(arc_iid);

	const auto cg_ptr = graph.getContainerGroup(a);

	const auto cg_iid = cg_shipment_num_to_cg_int_id_map.find(cg_ptr->getShipmentNumber())->second;
	auto & cg_ifc = data_interface_->container_group_ifcs_[cg_iid];
	//cg_ifc.cargo_exit_arc_id = arc_iid;

	graph_ifc.cargo_exit_arc_ids_.push_back(arc_iid);
	graph_ifc.arc_type_ids_from_arc_int_id_[arc_iid] = graph_ifc.cargo_exit_arc_ids_.size() - 1;
	graph_ifc.cg_id_of_cargo_exit_arcs_.push_back(cg_iid);
}
*/