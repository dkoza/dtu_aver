#include "inc/pt_data_interface.h"
#include "prototypes/common/inc/pt_common.h"

void PtDataInterface::writeDataToFiles(const std::string& dir) const
{
	std::ofstream output_file_;
	
	output_file_.open(dir + "/d_summary.txt", std::ios::out);
	printSummary(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_vessels.txt", std::ios::out);
	printVessels(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_sites.txt", std::ios::out);
	printSites(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_vessel_site_calls.txt", std::ios::out);
	printVesselSiteCalls(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_sailing_plans.txt", std::ios::out);
	printSailingPlans(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_sailing_legs.txt", std::ios::out);
	printSailingLegs(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_container_groups.txt", std::ios::out);
	printContainerGroups(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_transport_plans.txt", std::ios::out);
	printTransportPlans(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_transport_plan_legs.txt", std::ios::out);
	printTransportPlanLegs(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_connections.txt", std::ios::out);
	printConnections(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_avail_windows.txt", std::ios::out);
	printAvailabilityWindows(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_site_shifts.txt", std::ios::out);
	printSiteShifts(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_arcs_sailing.txt", std::ios::out);
	printGraphSailingArcs(output_file_);
	output_file_.close();

	output_file_.open(dir + "/d_arcs_port_stay.txt", std::ios::out);
	printGraphPortStayArcs(output_file_);
	output_file_.close();
	
	BOOST_LOG_TRIVIAL(info) << "  Data interface files have been written to '" << dir << "'";
}

double PtDataInterface::getContainerGroupUnitDelayPenalty(const PtContainerGroupInterface& cg_ifc,
	TArcIntId port_stay_arc_iid) const
{
	assert(graph_ifc_.getArcType(port_stay_arc_iid) == Graph::kARC_TYPE::kPORT_STAY);

	const auto node = graph_ifc_.getSinkNode(port_stay_arc_iid);
	const auto scr = graph_ifc_.graph_.getSiteCallRecord(node);
	
	return cost_calculator_.calculateCargoDelayPenalty(*cg_ifc.cg_ptr_, *scr);
}

void PtDataInterface::printSummary(std::ostream& os) const
{

	// General information
	size_t n_vsls_c = 0, n_vscs_c = 0, n_tps_c = 0, n_cnxs_c = 0, n_legs_c = 0, n_sail_arcs_c = 0, n_port_stay_arcs_c = 0, n_ts_arcs_c = 0;
	size_t n_tps_inf = 0, n_cnxs_inf = 0;

	for (const auto & vsl_ifc : getVesselInterfaces()) {
		n_vsls_c += vsl_ifc.isControllable();
	}
	for (const auto & vsc_ifc : getVesselSiteCallInterfaces()) {
		if (vsc_ifc.isControllable()) {
			n_vscs_c++;
			n_port_stay_arcs_c += vsc_ifc.getPortStayArcIds().size();
		}
	}
	for(const auto & tp_ifc : getTransportPlanInterfaces())
	{
		n_tps_c += tp_ifc.isControllable();
		n_tps_inf += !tp_ifc.isFeasible();
	}
	for (const auto & cnx_ifc : getConnectionInterfaces()) {
		if(getVesselSiteCallInterface(cnx_ifc.getVesselSiteCallFromId()).isControllable() || getVesselSiteCallInterface(cnx_ifc.getVesselSiteCallToId()).isControllable())
		{
			n_cnxs_c++;
			n_ts_arcs_c += cnx_ifc.getTransshipmentArcIds().size();
			n_cnxs_inf += !cnx_ifc.isFeasible();
		}
	}
	for (const auto & leg_ifc : getSailingLegInterfaces()) {
		if (leg_ifc.getArcIds().size() > 1) {
			n_legs_c++;
			n_sail_arcs_c += leg_ifc.getArcIds().size();
		}
	}

	os << ";n;n_controllable;n_infeasible\n";
	os << "vessels;" << getVesselInterfaces().size() << ";" << n_vsls_c << "\n";
	os << "vessel site calls;" << getVesselSiteCallInterfaces().size() << ";" << n_vscs_c << "\n";
	os << "sailing legs;" << getSailingLegInterfaces().size() << ";" << n_legs_c << "\n";
	os << "sailing plans;" << getSailingPlanInterfaces().size() << ";" << "\n";
	os << "connections;" << getConnectionInterfaces().size() << ";" << n_cnxs_c << ";" << n_cnxs_inf << "\n";
	os << "container groups;" << getContainerGroupInterfaces().size() << ";" << "\n";
	os << "transport plans;" << getTransportPlanInterfaces().size() << ";" << n_tps_c << ";" << n_tps_inf << "\n";
	//os << "nodes;"  << ";" << "\n";
	os << "sailing arcs;" << getGraphInterface().getSailingArcIds().size() << ";" << n_sail_arcs_c << "\n";
	os << "port stay arcs;" << getGraphInterface().getPortStayArcIds().size() << ";" << n_port_stay_arcs_c << "\n";
	os << "transshipment arcs;" << getGraphInterface().getTransshipmentArcIds().size() << ";" << n_ts_arcs_c << "\n";

}

void PtDataInterface::printVessels(std::ostream & os) const
{
	//os << "\n### Vessels ###\n";
	os << "vsl_id;name;is_controllable;n_sailing_plans;sailing_plan_ids\n";

	for (auto & vsl_ifc : vessel_ifcs_) {
		
		os << vsl_ifc.getId() << ";"
			<< vsl_ifc.getName() << ";"
			<< vsl_ifc.isControllable() << ";"
			<< vsl_ifc.getSailingPlanIds().size() << ";" 
			<< printVector(vsl_ifc.getSailingPlanIds()) << "\n";
	}
}

void PtDataInterface::printSites(std::ostream & os) const
{
	//os << "\n### Sites ###\n";
	os << "site_id;site_code;pilot_in_time;pilot_out_time;pilot_in_dist;pilot_out_dist;n_vsl_site_call_ids;vsl_site_call_ids;avail_window_ids\n";

	for (auto & site_ifc : site_ifcs_) {
		os << site_ifc.getId() << ";"
			<< site_ifc.getSiteCode() << ";"
			<< std::to_string(site_ifc.getPilotInDuration().hours()) << ";"
			<< std::to_string(site_ifc.getPilotOutDuration().hours()) << ";"
			<< site_ifc.getPilotInDistance() << ";"
			<< site_ifc.getPilotOutDistance() << ";"
			<< site_ifc.getVesselSiteCallIds().size() << ";"
			<< printVector(site_ifc.getVesselSiteCallIds()) << ";"
			<< printVector(site_ifc.getAvailabilityWindowIds()) << "\n";
	}
}

void PtDataInterface::printVesselSiteCalls(std::ostream & os) const
{
	//os << "\n### Vessel site calls ###\n";
	os << "vsl_site_call_id;vsl_id;site_id;name;can_control;can_omit;n_sailing_plans;n_transport_plans;sched_arr_time;sched_dep_time;actual_arr_time;actual_dep_time;earliest_arr_time;latest_arr_time;earliest_dep_time;latest_dep_time;n_port_stay_arcs;sailing_plan_ids;transport_plan_ids_load;transport_plan_ids_disch;avail_window_ids;avail_window_shift_ids\n";

	for (auto & vsc_ifc : vessel_site_call_ifcs_) {

		os << vsc_ifc.getId() << ";"
			<< vsc_ifc.getVesselId() << ";"
			<< vsc_ifc.getSiteId() << (vsc_ifc.getSiteId()==0 ? "-dummy" : "") << ";"
			<< vsc_ifc.getName() << ";"
			<< (vsc_ifc.isControllable() ? "yes" : "no") << ";"
			<< vsc_ifc.isOmissionAllowed() << ";"
			<< vsc_ifc.getSailingPlanIds().size() << ";"
			<< vsc_ifc.getTransportPlanIdsToLoad().size() + vsc_ifc.getTransportPlanIdsToDischarge().size() << ";"
			<< (vsc_ifc.wasScheduled() ? to_simple_string(vsc_ifc.getScheduledArrivalTime()) : "") << ";"
			<< (vsc_ifc.wasScheduled() ? to_simple_string(vsc_ifc.getScheduledDepartureTime()) : "") << ";"
			<< (vsc_ifc.isArrivalActualized() ? to_simple_string(vsc_ifc.getActualArrivalTime()) : "") << ";"
			<< (vsc_ifc.isDepartureActualized() ? to_simple_string(vsc_ifc.getActualDepartureTime()) : "") << ";"
			<< to_simple_string(vsc_ifc.getTimeSpanArr().begin()) << ";"
			<< to_simple_string(vsc_ifc.getTimeSpanArr().end()) << ";"
			<< to_simple_string(vsc_ifc.getTimeSpanDep().begin()) << ";"
			<< to_simple_string(vsc_ifc.getTimeSpanDep().end()) << ";" 
			<< vsc_ifc.getPortStayArcIds().size() << ";"
			<< printVector(vsc_ifc.getSailingPlanIds()) << ";"
			<< printVector(vsc_ifc.getTransportPlanIdsToLoad()) << ";"
			<< printVector(vsc_ifc.getTransportPlanIdsToDischarge()) << ";"
			<< printVector(vsc_ifc.getAvailabilityWindowIds()) << ";"
			<< printVector(vsc_ifc.getSiteShiftIds()) << "\n";
			
	}
}

void PtDataInterface::printSailingPlans(std::ostream & os) const
{
	//os << "\n### Sailing plans ###\n"; 
	os << "data_type;id;vsl_id;vsl_name;n_site_calls;compatible_tp_ids[leg_num];vsl_site_call_ids\n";

	for (auto & sp_ifc : sailing_plan_ifcs_) {
		
		std::string vessel_site_call_ids;
		std::string vessel_site_call_names;
		for (auto vessel_site_call_id : sp_ifc.getVesselSiteCallIds()) {
			vessel_site_call_ids += ";" + std::to_string(vessel_site_call_id);
			vessel_site_call_names += ";" + vessel_site_call_ifcs_[vessel_site_call_id].getName();
		}

		/*
		std::string compatible_tp_id_leg_nums;
		const auto & tp_id_leg_num_pairs = sp_ifc.getCompatibleTransportPlanIdLegNumPairs();
		for (auto tp_leg_num = 0; tp_leg_num < tp_id_leg_num_pairs.size(); tp_leg_num++) {
			if (tp_leg_num >= 25) {
				compatible_tp_id_leg_nums += ", ...";
				break;
			}
			compatible_tp_id_leg_nums += (tp_leg_num>0 ? ", " : "") + std::to_string(tp_id_leg_num_pairs[tp_leg_num].first) + "[" + std::to_string(tp_id_leg_num_pairs[tp_leg_num].second) + "]";
		}
		*/
		os << "ids;" << sp_ifc.getId()
			<< ";" << sp_ifc.getVesselId() << ";"
			<< vessel_ifcs_[sp_ifc.getVesselId()].getName() << ";"
			<< sp_ifc.getVesselSiteCallIds().size()// << ";"
			//<< compatible_tp_id_leg_nums
			<< vessel_site_call_ids << "\n";
		os << "txt;" << sp_ifc.getId() << ";"
			<< sp_ifc.getVesselId() << ";"
			<< vessel_ifcs_[sp_ifc.getVesselId()].getName() << ";"
			<< sp_ifc.getVesselSiteCallIds().size()// << ";"
			//<< compatible_tp_id_leg_nums
			<< vessel_site_call_names << "\n";
	}
}


void PtDataInterface::printSailingLegs(std::ostream & os) const
{
	//os << "\n### Sailing legs ###\n";
	os << "id;vsl_id;from_vsl_site_call_id;to_vsl_site_call_id;from_name;to_name;dist_ttl;dist_pilot;dist_sail;time_pilot;n_sailing_plans;sailing_plan_ids;num_arcs\n";

	for (auto & leg_ifc : sailing_leg_ifcs_) {

		const auto & site_from_ifc = getSiteInterface(getVesselSiteCallInterface(leg_ifc.getOriginVesselSiteCallId()).getSiteId());
		const auto & site_to_ifc = getSiteInterface(getVesselSiteCallInterface(leg_ifc.getDestinationVesselSiteCallId()).getSiteId());
		const double dist_pilot = site_from_ifc.getPilotOutDistance() + site_to_ifc.getPilotInDistance();
		const auto time_pilot = site_from_ifc.getPilotOutDuration() + site_to_ifc.getPilotInDuration();

		os << leg_ifc.getId() << ";"
			<< leg_ifc.getVesselId() << ";"
			<< leg_ifc.getOriginVesselSiteCallId() << ";"
			<< leg_ifc.getDestinationVesselSiteCallId() << ";"
			<< vessel_site_call_ifcs_[leg_ifc.getOriginVesselSiteCallId()].getName() << ";"
			<< vessel_site_call_ifcs_[leg_ifc.getDestinationVesselSiteCallId()].getName() << ";"
			<< leg_ifc.getDistance() << ";"
			<< (dist_pilot) << ";"
			<< (leg_ifc.getDistance() - dist_pilot) << ";"
			<< time_pilot << ";"
			<< leg_ifc.getSailingPlanIds().size() << ";"
			<< printVector(leg_ifc.getSailingPlanIds()) << ";"
			<< leg_ifc.getArcIds().size() << "\n";
	}
}


void PtDataInterface::printContainerGroups(std::ostream & os) const
{
	//os << "\n### Container Groups ###\n";
	os << "cg_id;shipment_no;n_teu;priority;type;agg_reroute_penalty;from_site_id;to_site_id;n_transport_plans;orig_tp_id;transport_plan_ids\n";// "orig_vtx_id;dest_vtx_id\n";

	for (auto & cg_ifc : container_group_ifcs_) {

		os << cg_ifc.getId() << ";"
			<< cg_ifc.getShipmentNumber() << ";"
			<< cg_ifc.getNumTEU() << ";"
			<< cg_ifc.getCustomerPriority() << ";"
			<< static_cast<int>(cg_ifc.getContainerType()) << ";"
			<< getContainerGroupAggregateReroutePenalty(cg_ifc) << ";"
			<< vessel_site_call_ifcs_[transport_plan_ifcs_[cg_ifc.getTransportPlanIds().front()].getLoadVesselSiteCallIds().front()].getSiteId() << ";"
			<< vessel_site_call_ifcs_[transport_plan_ifcs_[cg_ifc.getTransportPlanIds().front()].getDischargeVesselSiteCallIds().back()].getSiteId() << ";"
			<< cg_ifc.getTransportPlanIds().size() << ";"
			<< cg_ifc.getOriginalTransportPlanId() << ";"
			<< printVector(cg_ifc.getTransportPlanIds()) << "\n";
			//<< cg_ifc.getOriginVertexId() << ";"
			//<< cg_ifc.getDestinationVertexId()
	}
}


void PtDataInterface::printTransportPlans(std::ostream & os) const
{
	//os << "\n### Transport plans ###\n";
	os << "data_type;id;cg_id;is_orig;can_control;is_feasible;is_dominated;n_transshipments;min_delay;handling_cost;delay_penalty;vsl_site_call_load_disch_ids\n";

	for (auto & tp_ifc : transport_plan_ifcs_) {

		const auto & cg_ifc = getContainerGroupInterface(tp_ifc.getContainerGroupId());
		const auto & dest_vsc_ifc = getVesselSiteCallInterface(tp_ifc.getDestinationVesselSiteCallId());

		std::string vsl_site_call_ids;
		std::string vsl_site_call_names;
		for (int i = 0; i < tp_ifc.getNumLegs(); i++) {
			vsl_site_call_ids += ";" + std::to_string(tp_ifc.getLoadVesselSiteCallIds()[i]) + ";" + std::to_string(tp_ifc.getDischargeVesselSiteCallIds()[i]);
			vsl_site_call_names += ";" + vessel_site_call_ifcs_[tp_ifc.getLoadVesselSiteCallIds()[i]].getName() + ";" + vessel_site_call_ifcs_[tp_ifc.getDischargeVesselSiteCallIds()[i]].getName();
		}

		std::string delay_penalty = (dest_vsc_ifc.isControllable() ? "(variable)" : std::to_string(getContainerGroupUnitDelayPenalty(cg_ifc, dest_vsc_ifc.getPortStayArcIds().front())));

		// Print one line with IDs
		os << "ids;" << tp_ifc.getId() << ";"
			<< tp_ifc.getContainerGroupId() << ";"
			<< std::to_string(tp_ifc.isOriginal()) << ";"
			<< (tp_ifc.isControllable() ? "yes" : "no") << ";"
			<< (tp_ifc.isFeasible() ? "yes" : "no") << ";"
			<< (tp_ifc.isDominated() ? "yes" : "no") << ";"
			<< tp_ifc.getNumTransshipments() << ";"
			<< dest_vsc_ifc.getTimeSpanArr().begin() << ";"
			<< getTransportPlanUnitHandlingCost(tp_ifc) * cg_ifc.getNumTEU() << ";"
			<< delay_penalty
			<< vsl_site_call_ids << "\n";

		// Print one line with names written out (instead of IDs)
		os << "txt;" << tp_ifc.getId() << ";"
			<< tp_ifc.getContainerGroupId() << ";"
			<< std::to_string(tp_ifc.isOriginal()) << ";"
			<< (tp_ifc.isControllable() ? "yes" : "no") << ";"
			<< (tp_ifc.isFeasible() ? "yes" : "no") << ";"
			<< (tp_ifc.isDominated() ? "yes" : "no") << ";"
			<< tp_ifc.getNumTransshipments() << ";"
			<< dest_vsc_ifc.getTimeSpanArr().begin() << ";"
			<< getTransportPlanUnitHandlingCost(tp_ifc) * cg_ifc.getNumTEU() << ";"
			<< delay_penalty
			<< vsl_site_call_names << "\n";

	}
}

void PtDataInterface::printTransportPlanLegs(std::ostream & os) const
{
	//os << "\n### Transport plan legs ###\n";
	os << "tp_id;leg_num;cg_id;vsl_site_call_load_id;vsl_site_call_disch_id;vsl_site_call_load;vsl_site_call_disch;compatible_sp_ids\n";

	for (auto & tp_ifc : transport_plan_ifcs_) {

		for (size_t tp_leg_num = 0; tp_leg_num < tp_ifc.getNumTransshipments() + 1; tp_leg_num++)
		{
			const auto load_vsc_iid = tp_ifc.getLoadVesselSiteCallIds()[tp_leg_num];
			const auto disch_vsc_iid = tp_ifc.getDischargeVesselSiteCallIds()[tp_leg_num];

			os << tp_ifc.getId() << ";"
				<< tp_leg_num << ";"
				<< tp_ifc.getContainerGroupId() << ";"
				<< load_vsc_iid << ";"
				<< disch_vsc_iid << ";"
				<< vessel_site_call_ifcs_[load_vsc_iid].getName() << ";"
				<< vessel_site_call_ifcs_[disch_vsc_iid].getName() << ";"
				<< printVector(tp_ifc.getCompatibleSailingPlanIds(tp_leg_num)) << "\n";
		}
	}
}

void PtDataInterface::printConnections(std::ostream & os) const
{
	//os << "\n### Connections ###\n";
	os << "cnx_id;vsl_from_id;vsl_to_id;vsl_site_call_from_id;vsl_site_call_to_id;vsl_site_call_from;vsl_site_call_to;ctw_start;ctw_end;ctw_min_time;can_control;is_feasible;is_always_feasible;transport_plan_ids;orig_tp_ids;num_cg;teu_priority_1;teu_priority_2;teu_priority_3;teu_priority_4;teu_priority_5;teu_priority_6;num_arcs\n";

	for (auto & cnx_ifc : connection_ifcs_) {

		const auto & vsc_from_ifc = vessel_site_call_ifcs_[cnx_ifc.getVesselSiteCallFromId()];
		const auto & vsc_to_ifc = vessel_site_call_ifcs_[cnx_ifc.getVesselSiteCallToId()];

		std::string arr_dep_controllable = "no";
		if(vsc_from_ifc.isControllable())
		{
			arr_dep_controllable = "arr";
			if (vsc_to_ifc.isControllable())
				arr_dep_controllable += "+dep";
		} else if(vsc_to_ifc.isControllable())
		{
			arr_dep_controllable = "dep";
		}


		//std::string arr_dep_controllable = (vsc_from_ifc.isControllable() ? "arr" : "");
		//arr_dep_controllable += (vsc_to_ifc.isControllable() ? (vsc_from_ifc.isControllable() ? "+dep" : "dep") : (vsc_from_ifc.isControllable() ? "" : "no"));

		// Determine feasibility of connection, i.e. whether connection can be made
		const TTime & earliest_ctw_start = (cnx_ifc.getCtwStartType() == eSITE_CALL_TYPE::eARR ? vsc_from_ifc.getTimeSpanArr().begin() : vsc_from_ifc.getTimeSpanDep().begin());
		const TTime & latest_ctw_end = (cnx_ifc.getCtwEndType() == eSITE_CALL_TYPE::eARR ? vsc_to_ifc.getTimeSpanArr().end() : vsc_to_ifc.getTimeSpanDep().end());
		const bool is_feasible = (latest_ctw_end - earliest_ctw_start >= cnx_ifc.getMinimumConnectingTime());

		std::vector<size_t> original_transport_plan_ids;
		std::unordered_set<TContainerGroupIntId> container_group_ids;
		std::vector<float> num_teu_per_priority(6);

		auto & tp_ids = cnx_ifc.getTransportPlanIds();
		for(auto tp_id : tp_ids)
		{
			const auto & tp_ifc = transport_plan_ifcs_[tp_id];
			auto cg_id = tp_ifc.getContainerGroupId();
			
			if (container_group_ids.insert(cg_id).second)
				num_teu_per_priority[container_group_ifcs_[cg_id].getCustomerPriority()] += container_group_ifcs_[cg_id].getNumTEU();
			
			if (tp_ifc.isOriginal())
				original_transport_plan_ids.push_back(tp_id);
		}

		os << cnx_ifc.getId() << ";"
			<< vsc_from_ifc.getVesselId() << ";"
			<< vsc_to_ifc.getVesselId() << ";"
			<< cnx_ifc.getVesselSiteCallFromId() << ";"
			<< cnx_ifc.getVesselSiteCallToId() << ";"
			<< vsc_from_ifc.getName() << ";"
			<< vsc_to_ifc.getName() << ";"
			<< (cnx_ifc.getCtwStartType() == eSITE_CALL_TYPE::eARR ? "arr" : "dep") << ";"
			<< (cnx_ifc.getCtwEndType() == eSITE_CALL_TYPE::eARR ? "arr" : "dep") << ";"
			<< cnx_ifc.getMinimumConnectingTime() << ";"
			<< arr_dep_controllable << ";"
			<< (is_feasible ? "yes" : "no") << ";"
			<< (cnx_ifc.isAlwaysFeasible() ? "yes" : "no") << ";"
			<< printVector(tp_ids) << ";"
			<< printVector(original_transport_plan_ids) << ";"
			<< container_group_ids.size() << ";"
			<< num_teu_per_priority[0] << ";"
			<< num_teu_per_priority[1] << ";"
			<< num_teu_per_priority[2] << ";"
			<< num_teu_per_priority[3] << ";"
			<< num_teu_per_priority[4] << ";"
			<< num_teu_per_priority[5] << ";"
			<< cnx_ifc.getTransshipmentArcIds().size() << "\n";
	}
}

void PtDataInterface::printAvailabilityWindows(std::ostream& os) const
{
	//os << "\n### Availability windows ###\n";
	os << "aw_id;site_id;begin;end;is_limited_to_fuel_group;applies_to_vsl_site_call_ids;shift_ids\n";

	for (auto & aw_ifc : availability_window_ifcs_) {

		os << aw_ifc.getId() << ";"
			<< aw_ifc.getSiteId() << ";"
			<< to_simple_string(aw_ifc.getTimeSpan().begin()) << ";"
			<< to_simple_string(aw_ifc.getTimeSpan().end()) << ";"
			<< std::to_string(aw_ifc.isLimitedToFuelGroup()) << ";"
			<< printVector(aw_ifc.getVesselSiteCallIds()) << ";"
			<< printVector(aw_ifc.getSiteShiftIds()) << "\n";
	}
}

void PtDataInterface::printSiteShifts(std::ostream& os) const
{
	//os << "\n### Availability window shifts ###\n";
	os << "aw_shift_id;site_id;begin;end;n_avail_windows;avail_window_ids;applies_to_vsl_site_call_ids;port_stay_arc_ids\n";

	for (auto & site_shift_ifc : site_shift_ifcs_) {
		
		os << site_shift_ifc.getId() << ";"
			<< site_shift_ifc.getSiteId() << ";"
			<< to_simple_string(site_shift_ifc.getTimeSpan().begin()) << ";"
			<< to_simple_string(site_shift_ifc.getTimeSpan().end()) << ";"
			<< site_shift_ifc.getAvailabilityWindowIds().size() << ";"
			<< printVector(site_shift_ifc.getAvailabilityWindowIds()) << ";"
			<< printVector(site_shift_ifc.getVesselSiteCallIds()) << ";"
			<< printVector(site_shift_ifc.getPortStayArcIds()) << "\n";
	}
}

void PtDataInterface::printGraphSailingArcs(std::ostream& os) const
{
	os << "arc_id;vsl_id;leg_id;vsl_site_call_from_id;name_from;time_from;vsl_site_call_to_id;name_to;time_to;speed;cost\n";

	for (const auto arc_iid : graph_ifc_.getSailingArcIds()) {
		const auto & vsc_from_ifc = graph_ifc_.getVesselSiteCallFromInterface(arc_iid);
		const auto & vsc_to_ifc = graph_ifc_.getVesselSiteCallToInterface(arc_iid);
		os << arc_iid << ";"
			<< vsc_from_ifc.getVesselId() << ";"
			<< graph_ifc_.getSailingLegInterfaceFromSailingArc(arc_iid).getId() << ";"
			<< vsc_from_ifc.getId() << ";"
			<< vsc_from_ifc.getName() << ";"
			<< graph_ifc_.getArcTimeSpan(arc_iid).begin() << ";"
			<< vsc_to_ifc.getId() << ";"
			<< vsc_to_ifc.getName() << ";"
			<< graph_ifc_.getArcTimeSpan(arc_iid).end() << ";"
			<< graph_ifc_.getArcSpeed(arc_iid) << ";"
			<< graph_ifc_.getArcCost(arc_iid) << "\n";
	}
}

void PtDataInterface::printGraphPortStayArcs(std::ostream& os) const
{

	os << "arc_id;vsl_id;vsl_site_call_id;time_from;time_to;cost\n";

	for (const auto arc_iid : graph_ifc_.getPortStayArcIds()) {
		const auto & vsc_ifc = graph_ifc_.getVesselSiteCallInterfaceFromPortStayArc(arc_iid);
		os << arc_iid << ";"
			<< vsc_ifc.getVesselId() << ";"
			<< vsc_ifc.getId() << ";"
			<< graph_ifc_.getArcTimeSpan(arc_iid).begin() << ";"
			<< graph_ifc_.getArcTimeSpan(arc_iid).end() << ";"
			<< graph_ifc_.getArcCost(arc_iid)<< "\n";
	}
}

void PtDataInterface::printGraphVertices(std::ostream& os) const
{

}
