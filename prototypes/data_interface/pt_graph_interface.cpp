#include "inc/pt_graph_interface.h"
#include "inc/pt_data_interface.h"

const PtVesselInterface & PtGraphInterface::getVesselInterface(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kSAILING || getArcType(arc_iid) == Graph::kARC_TYPE::kPORT_STAY);

	const TVesselSiteCallIntId vsl_site_call_id = (getArcType(arc_iid) == Graph::kARC_TYPE::kPORT_STAY
		? vsl_site_call_id_of_port_stay_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]]
		: vsl_site_call_from_id_of_sailing_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]]
		);

	const TVesselIntId vsl_id = data_ifc_->getVesselSiteCallInterface(vsl_site_call_id).getVesselId();
	return data_ifc_->getVesselInterface(vsl_id);
}

/*
 * Function is probably expensive due to call of cost_calculator_.calculateSailing(...)
 */
double PtGraphInterface::getArcSpeed(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kSAILING);
	const auto & dg = graph_.getDigraph();
	const auto arc = dg.arcFromId(arc_iid);
	const auto & scr_from = graph_.getSiteCallRecord(dg.source(arc));
	const auto & scr_to = graph_.getSiteCallRecord(dg.target(arc));

	const auto & vsc_from_ifc = getVesselSiteCallFromInterface(arc_iid);
	const auto & vsc_to_ifc = getVesselSiteCallToInterface(arc_iid);
	const auto & site_from_ifc = data_ifc_->getSiteInterface(vsc_from_ifc.getSiteId());
	const auto & site_to_ifc = data_ifc_->getSiteInterface(vsc_to_ifc.getSiteId());

	const auto sail_duration = scr_to->getTime() - scr_from->getTime() - site_from_ifc.getPilotOutDuration() - site_to_ifc.getPilotInDuration();
	const auto sail_distance = getSailingLegInterfaceFromSailingArc(arc_iid).getDistance() - site_from_ifc.getPilotOutDistance() - site_to_ifc.getPilotInDistance();

	return sail_distance / (sail_duration.total_seconds() / 3600);
}

const std::vector<TSiteShiftIntId>& PtGraphInterface::getSiteShiftIdsOfPortStayArcId(
	TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kPORT_STAY);
	return site_shift_ids_of_port_stay_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
}

const PtVesselInterface & PtGraphInterface::getVesselInterfaceFromVertex(TVertexIntId vtx_iid) const
{
	assert(getVertexType(vtx_iid) == Graph::kNODE_TYPE::kVESSEL_ARR || getVertexType(vtx_iid) == Graph::kNODE_TYPE::kVESSEL_DEP);
	const TVesselIntId vsc_id = vsl_site_call_id_of_vessel_nodes_[vtx_type_ids_from_vtx_int_id_[vtx_iid]];
	return data_ifc_->getVesselInterface(data_ifc_->getVesselSiteCallInterface(vsc_id).getVesselId());
}

const PtVesselInterface & PtGraphInterface::getVesselInterfaceFromArc(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kPORT_STAY || getArcType(arc_iid) == Graph::kARC_TYPE::kSAILING);
	
	if (getArcType(arc_iid) == Graph::kARC_TYPE::kPORT_STAY)
	{
		const TVesselIntId vsc_id = vsl_site_call_id_of_port_stay_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
		return data_ifc_->getVesselInterface(data_ifc_->getVesselSiteCallInterface(vsc_id).getVesselId());
	}
	else {
		const TVesselIntId vsc_id = vsl_site_call_from_id_of_sailing_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
		return data_ifc_->getVesselInterface(data_ifc_->getVesselSiteCallInterface(vsc_id).getVesselId());
	}
}

const PtVesselSiteCallInterface& PtGraphInterface::getVesselSiteCallInterfaceFromVertex(TVertexIntId vtx_iid) const
{
	assert(getVertexType(vtx_iid) == Graph::kNODE_TYPE::kVESSEL_ARR || getVertexType(vtx_iid) == Graph::kNODE_TYPE::kVESSEL_DEP);
	const TVesselIntId vsc_id = vsl_site_call_id_of_vessel_nodes_[vtx_type_ids_from_vtx_int_id_[vtx_iid]];
	return data_ifc_->getVesselSiteCallInterface(vsc_id);
}

const PtVesselSiteCallInterface& PtGraphInterface::getVesselSiteCallInterfaceFromPortStayArc(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kPORT_STAY);
	const TVesselSiteCallIntId vsc_id = vsl_site_call_id_of_port_stay_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
	return data_ifc_->getVesselSiteCallInterface(vsc_id);
}

const PtSailingLegInterface& PtGraphInterface::getSailingLegInterfaceFromSailingArc(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kSAILING);
	const TLegIntId sl_id = sailing_leg_id_of_sailing_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
	return data_ifc_->getSailingLegInterface(sl_id);
}

const PtVesselSiteCallInterface& PtGraphInterface::getVesselSiteCallFromInterface(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kSAILING);
	const TVesselSiteCallIntId vsc_id = vsl_site_call_from_id_of_sailing_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
	return data_ifc_->getVesselSiteCallInterface(vsc_id);
}

const PtVesselSiteCallInterface& PtGraphInterface::getVesselSiteCallToInterface(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kSAILING);
	const TVesselSiteCallIntId vsc_id = vsl_site_call_to_id_of_sailing_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
	return data_ifc_->getVesselSiteCallInterface(vsc_id);
}

const PtConnectionInterface& PtGraphInterface::getConnectionInterface(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kTRANSSHIPMENT);
	const TConnectionIntId cnx_id = connection_id_of_transshipment_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]];
	return data_ifc_->getConnectionInterface(cnx_id);
}

const PtContainerGroupInterface& PtGraphInterface::getContainerGroupInterface(TArcIntId arc_iid) const
{
	assert(getArcType(arc_iid) == Graph::kARC_TYPE::kCARGO_ENTRY || getArcType(arc_iid) == Graph::kARC_TYPE::kCARGO_EXIT);

	const TContainerGroupIntId cg_id = (getArcType(arc_iid) == Graph::kARC_TYPE::kCARGO_ENTRY
		? cg_id_of_cargo_entry_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]]
		: cg_id_of_cargo_exit_arcs_[arc_type_ids_from_arc_int_id_[arc_iid]]
		);

	return data_ifc_->getContainerGroupInterface(cg_id);
}
