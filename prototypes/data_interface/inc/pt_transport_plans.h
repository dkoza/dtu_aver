#pragma once

#include "common_types.h"
#include "domain/inc/container_group.h"


class PtTransportPlanInterface
{
	friend class PtDataInterfaceBuilder;
	friend class PtDataInterface;

public:
	PtTransportPlanInterface(TTransportPlanIntId id, TContainerGroupIntId cg_id, const TContainerGroupTransportPlanConstPtr tp)
		: id_(id)
		, container_group_id_(cg_id)
		, tp_(tp)
		, can_control_(false)
		, is_dominated_(false)
		, is_feasible_(true)
	{}

	TTransportPlanIntId getId() const { return id_; }
	TContainerGroupIntId getContainerGroupId() const { return container_group_id_; }
	size_t getNumTransshipments() const { return load_vessel_site_call_ids_.size() - 1; }
	size_t getNumLegs() const { return load_vessel_site_call_ids_.size(); }
	bool isOriginal() const { return tp_->isOriginal(); }
	bool isControllable() const { return can_control_; }
	bool isDominated() const { return is_dominated_; }
	bool isFeasible() const { return is_feasible_; }

	//const std::vector<TVesselIntId> & getVesselIds() const { return vessel_ids_; }
	const std::vector<TVesselSiteCallIntId> & getLoadVesselSiteCallIds() const { return load_vessel_site_call_ids_; }
	const std::vector<TVesselSiteCallIntId> & getDischargeVesselSiteCallIds() const { return discharge_vessel_site_call_ids_; }
	TVesselSiteCallIntId getOriginVesselSiteCallId() const { return load_vessel_site_call_ids_.front(); }
	TVesselSiteCallIntId getDestinationVesselSiteCallId() const { return discharge_vessel_site_call_ids_.back(); }
	const std::vector<TSailingPlanIntId> & getCompatibleSailingPlanIds(size_t leg_num) const { return compatible_sailing_plans_per_transport_leg_[leg_num]; }
	const std::vector<TConnectionIntId> & getConnectionIds() const { return connection_ids_; }

	std::string asString() const { return tp_->asString(); }

private:

	TTransportPlanIntId id_;
	TContainerGroupIntId container_group_id_;
	const TContainerGroupTransportPlanConstPtr tp_;

	bool can_control_;	// false, if all connections are non-controllable
	bool is_dominated_;	// A transport plan is dominated, if (1) it is not controllable, (2) a cheaper, non-controllable TP exists
	bool is_feasible_;	// By default assumed to be true; false, if e.g. a used connection is not feasible

	// sequence of vessel IDs used by transport plan // Not needed, as vessel information is part of vessel_site_call_ids
	//std::vector<TVesselIntId> vessel_ids_;

	// sequence of load vessel site call IDs used by transport plan
	std::vector<TVesselSiteCallIntId> load_vessel_site_call_ids_;
	// sequence of discharge vessel site call IDs used by transport plan
	std::vector<TVesselSiteCallIntId> discharge_vessel_site_call_ids_;
	// sequence of connection IDs used by transport plan
	std::vector<TConnectionIntId> connection_ids_;

	std::vector <std::vector<TSailingPlanIntId>> compatible_sailing_plans_per_transport_leg_;

};