#pragma once


typedef size_t TVesselIntId;
typedef size_t TSiteIntId;
typedef size_t TVesselSiteCallIntId;
typedef size_t TLegIntId;	// TODO: Rename to TSailingLegIntId
typedef size_t TContainerGroupIntId;
typedef size_t TSailingPlanIntId;
typedef size_t TTransportPlanIntId;
typedef size_t TConnectionIntId;
typedef size_t TVertexIntId;
typedef size_t TArcIntId;
typedef size_t TPortStayArcIntId;
typedef size_t TAvailabilityWindowIntId;
typedef size_t TSiteShiftIntId;