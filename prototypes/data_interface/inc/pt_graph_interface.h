#pragma once

#include "common_types.h"
#include "graph/inc/graph.h"
#include "pt_data_interface_defs.h"
#include "pt_connections.h"
#include "pt_container_groups.h"
#include "pt_legs.h"

class PtDataInterface;
class PtVesselInterface;
class PtVesselSiteCallInterface;

class PtGraphInterface
{
	friend class PtDataInterfaceBuilder;
	friend class PtDataInterface;

public:

	PtGraphInterface(const PtDataInterface * data_ifc, const Graph & graph)
		: data_ifc_(data_ifc)
		, graph_(graph)
		, dg_(graph.getDigraph())
		, vtx_type_ids_from_vtx_int_id_(dg_.nodeNum(), -1)
		, arc_type_ids_from_arc_int_id_(dg_.arcNum(), -1)
	{}

	const std::vector<TArcIntId> & getSailingArcIds() const { return sailing_arc_ids_; }
	const std::vector<TArcIntId> & getTransshipmentArcIds() const { return transshipment_arc_ids_; }
	const std::vector<TArcIntId> & getPortStayArcIds() const { return port_stay_arc_ids_; }

	const lemon::SmartDigraph & getDigraph() const { return dg_; }
	size_t getId(Graph::Arc a) const { return dg_.id(a); }
	size_t getId(Graph::Node v) const { return dg_.id(v); }

	Graph::kNODE_TYPE getVertexType(TVertexIntId vtx_iid) const { return graph_.getNodeType(dg_.nodeFromId(vtx_iid)); }
	Graph::kNODE_TYPE getVertexType(Graph::Node v) const { return graph_.getNodeType(v); }

	Graph::kARC_TYPE getArcType(TArcIntId arc_iid) const { return graph_.getArcType(dg_.arcFromId(arc_iid)); }
	Graph::kARC_TYPE getArcType(Graph::Arc a) const { return graph_.getArcType(a); }

	TTime getVertexTime(TVertexIntId vtx_iid) const { return graph_.getSiteCallRecord(dg_.nodeFromId(vtx_iid))->getTime(); }

	double getArcCost(TArcIntId arc_iid) const { return graph_.getCost(dg_.arcFromId(arc_iid)); }
	double getArcCost(Graph::Arc a) const { return graph_.getCost(a); }
	//double getArcBunkerCost(TArcIntId arc_iid) const {return }

	double getArcSpeed(TArcIntId arc_iid) const;

	TTimePeriod getArcTimeSpan(TArcIntId arc_iid) const { return TTimePeriod(graph_.getSiteCallRecord(dg_.source(dg_.arcFromId(arc_iid)))->getTime(), graph_.getSiteCallRecord(dg_.target(dg_.arcFromId(arc_iid)))->getTime()); }

	size_t getSourceNodeId(TArcIntId arc_iid) const { return dg_.id(dg_.source(dg_.arcFromId(arc_iid))); }
	size_t getSinkNodeId(TArcIntId arc_iid) const { return dg_.id(dg_.target(dg_.arcFromId(arc_iid))); }
	Graph::Node getSourceNode(TArcIntId arc_iid) const { return dg_.source(dg_.arcFromId(arc_iid)); }
	Graph::Node getSinkNode(TArcIntId arc_iid) const { return dg_.target(dg_.arcFromId(arc_iid)); }

	const std::vector<TSiteShiftIntId> & getSiteShiftIdsOfPortStayArcId(TArcIntId arc_iid) const;

	const PtVesselInterface& getVesselInterfaceFromVertex(TVertexIntId vtx_iid) const;
	const PtVesselInterface& getVesselInterfaceFromArc(TArcIntId arc_iid) const;
	const PtVesselSiteCallInterface& getVesselSiteCallInterfaceFromVertex(TVertexIntId vtx_iid) const;

	// TODO: Rename below by adding "FromArc" to function name
	const PtVesselInterface& getVesselInterface(TArcIntId arc_iid) const;
	const PtVesselSiteCallInterface& getVesselSiteCallInterfaceFromPortStayArc(TArcIntId arc_iid) const;
	const PtSailingLegInterface& getSailingLegInterfaceFromSailingArc(TArcIntId arc_iid) const;
	const PtVesselSiteCallInterface& getVesselSiteCallFromInterface(TArcIntId arc_iid) const;
	const PtVesselSiteCallInterface& getVesselSiteCallToInterface(TArcIntId arc_iid) const;
	const PtConnectionInterface& getConnectionInterface(TArcIntId arc_iid) const;
	const PtContainerGroupInterface& getContainerGroupInterface(TArcIntId arc_iid) const;

private:

	const PtDataInterface * data_ifc_;

	const Graph & graph_;
	const lemon::SmartDigraph & dg_;

	// Maps TVertexIntId to TVesselVertexIntId etc.
	std::vector<size_t> vtx_type_ids_from_vtx_int_id_;

	// Maps TArcIntId to TSailingArcIntId / TPortStayArcIntId / TTransshipmentArcIntId / etc
	std::vector<size_t> arc_type_ids_from_arc_int_id_;

	std::vector<TVertexIntId> vessel_vtx_ids_;
	std::vector<TVesselSiteCallIntId> vsl_site_call_id_of_vessel_nodes_;

	

	std::vector<TArcIntId> sailing_arc_ids_;
	std::vector<TLegIntId> sailing_leg_id_of_sailing_arcs_;
	std::vector<TVesselSiteCallIntId> vsl_site_call_from_id_of_sailing_arcs_;
	std::vector<TVesselSiteCallIntId> vsl_site_call_to_id_of_sailing_arcs_;
	
	std::vector<TArcIntId> transshipment_arc_ids_;	// IDs of transshipment arcs that are used by at least one connection. Note that a connection is only added, if it is used by at least one transport plan
	std::vector<TConnectionIntId> connection_id_of_transshipment_arcs_;	// TConnectionIntId of transshipment arcs defined in 'transshipment_arc_ids_'

	std::vector<TArcIntId> port_stay_arc_ids_;
	std::vector<TVesselSiteCallIntId> vsl_site_call_id_of_port_stay_arcs_;	// TVesselSiteCallIds of port stay arcs defined in 'port_stay_arc_ids_'
	std::vector<std::vector<TSiteShiftIntId>> site_shift_ids_of_port_stay_arcs_;	// Vector of SiteShiftIntIds of port stay arcs

	std::vector<TArcIntId> cargo_entry_arc_ids_;
	std::vector<TContainerGroupIntId> cg_id_of_cargo_entry_arcs_;	// TContainerGroupIntId of cargo entry arcs defined in 'cargo_entry_arc_ids_'

	std::vector<TArcIntId> cargo_exit_arc_ids_;
	std::vector<TContainerGroupIntId> cg_id_of_cargo_exit_arcs_;	// TContainerGroupIntId of cargo exit arcs defined in 'cargo_exit_arc_ids_'
};
