#pragma once

#include "common_types.h"
#include "pt_data_interface_defs.h"
#include "domain/inc/site_call.h"
#include "domain/inc/vessel.h"

/*
 * Interface that represents (TVesselID, TSiteCallKey) objects.
 */
class PtVesselSiteCallInterface
{

	friend class PtDataInterfaceBuilder;

public:
	PtVesselSiteCallInterface(TVesselSiteCallIntId id, TVesselIntId vessel_id, TSiteIntId site_id, const TConstVesselPtr & vsl_ptr, const TSiteCallKey & site_call_key, bool is_omission_allowed)
		: id_(id)
		, vessel_id_(vessel_id)
		, site_id_(site_id)
		, vsl_ptr_(vsl_ptr)
		, site_call_key_(site_call_key)
		, scheduled_port_stay_(nullptr)
		, is_omission_allowed_(is_omission_allowed)
		, time_span_arr_(boost::posix_time::ptime(boost::posix_time::pos_infin), boost::posix_time::ptime(boost::posix_time::neg_infin))
		, time_span_dep_(boost::posix_time::ptime(boost::posix_time::pos_infin), boost::posix_time::ptime(boost::posix_time::neg_infin))
	{
	}

	TVesselSiteCallIntId getId() const { return id_; }
	std::string getName() const { return vsl_ptr_->getVesselCode() + "/" + site_call_key_.asString(); }
	TVesselIntId getVesselId() const { return vessel_id_; }
	TSiteIntId getSiteId() const { return site_id_; }
	const TSiteCallKey & getSiteCallKey() const { return site_call_key_; }
	bool isControllable() const { return (vsl_ptr_->isScenarioVessel() && (port_stay_arc_ids_.size() > 1 || is_omission_allowed_)); }
	bool isOmissionAllowed() const { return is_omission_allowed_; }

	const std::vector<TSailingPlanIntId> & getSailingPlanIds() const { return sailing_plan_ids_; }
	const std::vector<TTransportPlanIntId> & getTransportPlanIdsToLoad() const { return transport_plan_load_ids_; }
	const std::vector<TTransportPlanIntId> & getTransportPlanIdsToDischarge() const { return transport_plan_discharge_ids_; }
	const std::vector<TTransportPlanIntId> & getFinalDestinationTransportPlanIds() const { return transport_plan_destination_ids_; }
	const std::vector<TAvailabilityWindowIntId> & getAvailabilityWindowIds() const { return availability_window_ids_; }
	const std::vector<TSiteShiftIntId> & getSiteShiftIds() const { return site_shift_ids_; }
	const std::vector<TArcIntId> & getPortStayArcIds() const { return port_stay_arc_ids_; }
	const TTimePeriod getTimeSpan() const { return boost::posix_time::time_period(time_span_arr_.begin(), time_span_dep_.end()); }
	const TTimePeriod & getTimeSpanArr() const { return time_span_arr_; }
	const TTimePeriod & getTimeSpanDep() const { return time_span_dep_; }

	bool wasScheduled() const { return scheduled_port_stay_ != nullptr; }
	bool isArrivalActualized() const { return scheduled_port_stay_->getOrig().isActual(); }
	bool isDepartureActualized() const { return scheduled_port_stay_->getDest().isActual(); }
	const TTime& getScheduledArrivalTime() const { return (wasScheduled() ? scheduled_port_stay_->getOrig().getScheduledTime() : TTime()); }
	const TTime& getScheduledDepartureTime() const { return (wasScheduled() ? scheduled_port_stay_->getDest().getScheduledTime() : TTime()); }

	const TTime& getActualArrivalTime() const { return (isArrivalActualized() ? scheduled_port_stay_->getOrig().getTime() : TTime()); }
	const TTime& getActualDepartureTime() const { return (isDepartureActualized() ? scheduled_port_stay_->getDest().getTime() : TTime()); }


private:

	TVesselSiteCallIntId id_;
	TVesselIntId vessel_id_;
	TSiteIntId site_id_;
	const TConstVesselPtr vsl_ptr_;
	const TSiteCallKey & site_call_key_;
	const PortStay * scheduled_port_stay_; // Pointer to object that stores information about original schedule (nullptr if vessel site call is not originally planned, but e.g. induced)

	bool is_omission_allowed_;

	std::vector<TSailingPlanIntId> sailing_plan_ids_;	// IDs of sailing plans that include this vessel site call
	
	std::vector<TTransportPlanIntId> transport_plan_load_ids_;		// IDs of transport plans for which vessel site call is load call
	std::vector<TTransportPlanIntId> transport_plan_discharge_ids_;	// IDs of transport plans for which vessel site call is discharge call
	std::vector<TTransportPlanIntId> transport_plan_destination_ids_;	// IDs of transport plans for which vessel site call is final destination call

	std::vector<TAvailabilityWindowIntId> availability_window_ids_;	// IDs of availability windows that can be used by this vessel_site_call
	std::vector<TSiteShiftIntId> site_shift_ids_;	// IDs of availability window shifts that can be used by this vessel_site_call

	TTimePeriod time_span_arr_;	// Time span during which site call can start, i.e. during which vessel can arrive (based on existence of port stay arcs) 
	TTimePeriod time_span_dep_;	// Time span during which site call can end, i.e. during which vessel can depart (based on existence of port stay arcs) 
	std::vector<TArcIntId> port_stay_arc_ids_;	// IDs of port stay arcs associated with this vessel site call

	//std::vector<size_t> origin_connection_ids_;
	//std::vector<size_t> destination_connection_ids_;

	//std::vector<TTransportPlanIntId> transport_plan_ts_unload_ids_;	// IDs of transport plans for which vessel site call is transshipment unload call
	//std::vector<TTransportPlanIntId> transport_plan_ts_load_ids_;	// IDs of transport plans for which vessel site call is transshipment load call


};