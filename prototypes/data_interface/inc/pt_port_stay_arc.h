#pragma once

#include "common_types.h"
#include "pt_data_interface_defs.h"

class PtArcPortStay
{
	friend class PtDataInterfaceBuilder;

public:
	PtArcPortStay(TArcIntId id, TVesselIntId vessel_id, TVesselSiteCallIntId vessel_site_call_id)
		: id_(id)
		, vessel_id_(vessel_id)
		, vessel_site_call_id_(vessel_site_call_id)
	{
	}

	TArcIntId getId() const { return id_; }
	TVesselIntId getVesselId() const { return vessel_id_; }
	TVesselSiteCallIntId getVesselSiteCallId() const { return vessel_site_call_id_; }
	TTimePeriod getArcTimeSpan(TArcIntId arc_iid) const { return TTimePeriod(graph_.getSiteCallRecord(dg_.source(dg_.arcFromId(arc_iid)))->getTime(), graph_.getSiteCallRecord(dg_.target(dg_.arcFromId(arc_iid)))->getTime()); }

	const std::vector<TSiteShiftIntId> & getSiteShiftIds() const { return site_shift_ids_; }

private:
	TArcIntId id_;
	TVesselIntId vessel_id_;
	TVesselSiteCallIntId vessel_site_call_id_;
	const std::vector<TSiteShiftIntId> site_shift_ids_;

};
