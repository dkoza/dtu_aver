#pragma once

#include "common_types.h"
#include "domain/inc/connecting_time_window.h"

class PtConnectionInterface
{
	friend class PtDataInterfaceBuilder;

public:
	PtConnectionInterface(TConnectionIntId id, const ConnectingTimeWindow & ctw, TVesselSiteCallIntId vessel_site_call_from_id, TVesselSiteCallIntId vessel_site_call_to_id)
		: id_(id)
		, ctw_(ctw)
		, vessel_site_call_from_id_(vessel_site_call_from_id)
		, vessel_site_call_to_id_(vessel_site_call_to_id)
		, can_control_(false)
		, is_always_feasible_(false)
	{}

	TConnectionIntId getId() const { return id_; }
	TVesselSiteCallIntId getVesselSiteCallFromId() const { return vessel_site_call_from_id_; }
	TVesselSiteCallIntId getVesselSiteCallToId() const { return vessel_site_call_to_id_; }
	bool isControllable() const { return can_control_; }
	bool isFeasible() const { return (!arc_ids_.empty()); }
	bool isAlwaysFeasible() const { return is_always_feasible_; }

	const std::vector<TTransportPlanIntId> & getTransportPlanIds() const { return transport_plan_ids_; }
	const std::vector<TArcIntId> & getTransshipmentArcIds() const { return arc_ids_; }	// Returns transshipment arcs of this connection
	eSITE_CALL_TYPE getCtwStartType() const { return ctw_.getCountingFrom(); }
	eSITE_CALL_TYPE getCtwEndType() const { return ctw_.getCountingTo(); }
	const TTimeDuration & getMinimumConnectingTime() const { return ctw_.getTimeWindow(); }

private:

	TConnectionIntId id_;
	const ConnectingTimeWindow & ctw_;
	TVesselSiteCallIntId vessel_site_call_from_id_;
	TVesselSiteCallIntId vessel_site_call_to_id_;
	bool can_control_;
	bool is_always_feasible_;

	std::vector<TTransportPlanIntId> transport_plan_ids_;	// IDs of transport plans that use connection
	std::vector<TArcIntId> arc_ids_;	// transshipment arcs that represent this connection
};
