#pragma once

#include "common_types.h"
#include "domain/inc/site.h"
#include "pt_data_interface_defs.h"

class PtSiteInterface
{
	friend class PtDataInterfaceBuilder;

public:

	PtSiteInterface(TSiteIntId id, const Site * site)
		: id_(id)
		, site_(site)
	{}

	TSiteIntId getId() const { return id_; }
	TSiteCode getSiteCode() const { return (site_!=nullptr ? site_->getSiteCode() : "dummy"); }
	std::vector<TVesselSiteCallIntId> getVesselSiteCallIds() const { return vessel_site_call_ids_; }
	std::vector<TAvailabilityWindowIntId> getAvailabilityWindowIds() const { return availability_window_ids_; }

	bool isDummySite() const { return id_ == 0; }

	TTimeDuration getPilotOutDuration() const { return (site_ != nullptr ? site_->getPilotage().getPilotingOut().getDuration() : TTimeDuration(0,0,0,0)); }
	TTimeDuration getPilotInDuration() const { return (site_ != nullptr ? site_->getPilotage().getPilotingIn().getDuration() : TTimeDuration(0, 0, 0, 0)); }
	double getPilotOutDistance() const { return (site_ != nullptr ? site_->getPilotage().getPilotingOut().getDistance() : 0.0); }
	double getPilotInDistance() const { return (site_ != nullptr ? site_->getPilotage().getPilotingIn().getDistance() : 0.0); }

private:

	TSiteIntId id_;
	const Site * site_;

	std::vector<TVesselSiteCallIntId> vessel_site_call_ids_;	// IDs of vessel site calls at this site
	std::vector<TAvailabilityWindowIntId> availability_window_ids_;	// IDs of availability windows at this site
};

