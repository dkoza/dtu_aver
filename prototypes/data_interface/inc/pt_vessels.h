#pragma once

#include "common_types.h"
#include "domain/inc/vessel.h"
#include "pt_data_interface_defs.h"

class PtVesselInterface
{
	friend class PtDataInterfaceBuilder;

public:

	PtVesselInterface(TVesselIntId id, const TConstVesselPtr & vsl_ptr)
		: id_(id)
		, vsl_ptr_(vsl_ptr)
		, source_vtx_id_(-1)	// initialized with large size_t value
		, sink_vtx_id_(-1)		// initialized with large size_t value
	{
	}

	TVesselIntId getId() const { return id_; }
	const TVesselCode & getName() const { return vsl_ptr_->getVesselCode(); }
	bool isControllable() const { return vsl_ptr_->isScenarioVessel(); }
	const std::vector<TSailingPlanIntId> & getSailingPlanIds() const { return sailing_plan_ids_; }
	const std::vector<TVesselSiteCallIntId> & getVesselSiteCallIds() const {return vessel_site_call_ids_;}

	size_t getSourceNodeId() const { return source_vtx_id_; }
	size_t getSinkNodeId() const { return sink_vtx_id_; }

private:
	
	TVesselIntId id_;
	const TConstVesselPtr vsl_ptr_;

	//std::vector<size_t> planned_vessel_site_call_ids_;
	//std::vector<size_t> inducement_vessel_site_call_ids_;

	std::vector<TSailingPlanIntId> sailing_plan_ids_;
	std::vector<TVesselSiteCallIntId> vessel_site_call_ids_;

	size_t source_vtx_id_;
	size_t sink_vtx_id_;
};
