#pragma once

#include "common_types.h"
#include "domain/inc/container_group.h"
#include "pt_data_interface_defs.h"

class PtContainerGroupInterface
{
	friend class PtDataInterfaceBuilder;
	friend class PtDataInterface;

public:
	PtContainerGroupInterface(TContainerGroupIntId id, const TContainerGroupPtr & cg_ptr)
		: id_(id)
		, cg_ptr_(cg_ptr)
		, origin_vtx_id_(-1)				// initialize with dummy value
		, destination_vtx_id_(-1)			// initialize with dummy value
		, original_transport_plan_id_(-1)	// initialize with dummy value
	{}

	TContainerGroupIntId getId() const { return id_; }
	TShipmentNumber getShipmentNumber() const { return cg_ptr_->getShipmentNumber(); }
	float getNumTEU() const { return cg_ptr_->getTEU(); }
	int getCustomerPriority() const { return cg_ptr_->getCustomerPriority(); }
	kCONTAINER_TYPE getContainerType() const { return cg_ptr_->getContainerType(); }

	size_t getOriginVertexId() const { return origin_vtx_id_; }
	size_t getDestinationVertexId() const { return destination_vtx_id_; }

	TTransportPlanIntId getOriginalTransportPlanId() const { return original_transport_plan_id_; }
	const std::vector<TTransportPlanIntId> & getTransportPlanIds() const { return transport_plan_ids_; }

private:

	TContainerGroupIntId id_;
	const TContainerGroupPtr cg_ptr_;

	size_t origin_vtx_id_;
	size_t destination_vtx_id_;

	std::vector<TTransportPlanIntId> transport_plan_ids_;
	TTransportPlanIntId original_transport_plan_id_;
};
