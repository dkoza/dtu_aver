#pragma once

#include "common_types.h"
#include "pt_data_interface_defs.h"

class PtSailingLegInterface
{
	friend class PtDataInterfaceBuilder;

public:

	PtSailingLegInterface(TLegIntId id, TVesselIntId vessel_id, TVesselSiteCallIntId origin_vessel_site_call_id, TVesselSiteCallIntId destination_vessel_site_call_id)
		: id_(id)
		, vessel_id_(vessel_id)
		, origin_vessel_site_call_id_(origin_vessel_site_call_id)
		, destination_vessel_site_call_id_(destination_vessel_site_call_id)
		, can_control_(false)
		, distance_(0.0)
	{
	}

	TLegIntId getId() const { return id_; }
	TVesselIntId getVesselId() const { return vessel_id_; }
	TVesselSiteCallIntId getOriginVesselSiteCallId() const { return origin_vessel_site_call_id_; }
	TVesselSiteCallIntId getDestinationVesselSiteCallId() const { return destination_vessel_site_call_id_; }
	bool isControllable() const { return can_control_; }

	const std::vector<TSailingPlanIntId> & getSailingPlanIds() const { return sailing_plan_ids_; }
	const std::vector<TArcIntId> & getArcIds() const { return arc_ids_; }

	double getDistance() const { return distance_; }

private:

	TLegIntId id_;
	TVesselIntId vessel_id_;
	TVesselSiteCallIntId origin_vessel_site_call_id_;
	TVesselSiteCallIntId destination_vessel_site_call_id_;
	bool can_control_;

	double distance_;	// berth-to-berth distance, including pilot in/out distance

	// IDs of sailing plans that use this sailing leg
	std::vector<TSailingPlanIntId> sailing_plan_ids_;

	// Arc IDs that represent sailing leg
	std::vector<TArcIntId> arc_ids_;
};
