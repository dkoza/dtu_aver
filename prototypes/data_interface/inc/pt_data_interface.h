#pragma once

#include "common_types.h"
#include "pt_data_interface_defs.h"

#include "pt_sites.h"
#include "pt_vessel_site_calls.h"
#include "pt_vessels.h"
#include "pt_container_groups.h"
#include "pt_legs.h"
#include "pt_connections.h"
#include "pt_transport_plans.h"
#include "pt_sailing_plans.h"
#include "pt_availability_windows.h"
#include "pt_site_shift_interface.h"
#include "pt_graph_interface.h"

#include "rule_engine/inc/cost_calculator.h"


class PtDataInterface
{

	friend class PtDataInterfaceBuilder;
	friend class PtGraphInterface;

public:

	PtDataInterface(const Graph & graph, const CostCalculator & cost_calculator)
		: graph_ifc_(this, graph)
		, cost_calculator_(cost_calculator)
	{}

	void writeDataToFiles(const std::string & dir) const;

	const PtGraphInterface & getGraphInterface() const { return graph_ifc_; }
	const PtSiteInterface & getSiteInterface(TSiteIntId site_iid) const { return site_ifcs_[site_iid]; }
	const PtVesselInterface & getVesselInterface(TVesselIntId vsl_iid) const { return vessel_ifcs_[vsl_iid]; }
	const PtVesselSiteCallInterface & getVesselSiteCallInterface(TVesselSiteCallIntId vsl_site_call_iid) const { return vessel_site_call_ifcs_[vsl_site_call_iid]; }
	const PtSailingLegInterface & getSailingLegInterface(TLegIntId sl_iid) const { return sailing_leg_ifcs_[sl_iid]; }
	const PtSailingPlanInterface & getSailingPlanInterface(TSailingPlanIntId sp_iid) const { return sailing_plan_ifcs_[sp_iid]; }
	const PtConnectionInterface & getConnectionInterface(TConnectionIntId cnx_iid) const { return connection_ifcs_[cnx_iid]; }
	const PtContainerGroupInterface & getContainerGroupInterface(TContainerGroupIntId cg_iid) const { return container_group_ifcs_[cg_iid]; }
	const PtTransportPlanInterface & getTransportPlanInterface(TTransportPlanIntId tp_iid) const { return transport_plan_ifcs_[tp_iid]; }
	const PtAvailabilityWindowInterface & getAvailabilityWindowInterface(TAvailabilityWindowIntId aw_iid) const { return availability_window_ifcs_[aw_iid]; }
	const PtSiteShiftInterface & getSiteShiftInterface(TSiteShiftIntId aws_iid) const { return site_shift_ifcs_[aws_iid]; }

	const std::vector<PtVesselInterface> & getVesselInterfaces() const {return vessel_ifcs_; }
	const std::vector<PtVesselSiteCallInterface> & getVesselSiteCallInterfaces() const { return vessel_site_call_ifcs_; }
	const std::vector<PtSailingLegInterface> & getSailingLegInterfaces() const { return sailing_leg_ifcs_; }
	const std::vector<PtConnectionInterface> & getConnectionInterfaces() const { return connection_ifcs_; }
	const std::vector<PtContainerGroupInterface> & getContainerGroupInterfaces() const { return container_group_ifcs_; }
	const std::vector<PtTransportPlanInterface> & getTransportPlanInterfaces() const { return transport_plan_ifcs_; }
	const std::vector<PtAvailabilityWindowInterface> & getAvailabilityWindowInterfaces() const { return availability_window_ifcs_; }
	const std::vector<PtSailingPlanInterface> & getSailingPlanInterfaces() const { return sailing_plan_ifcs_; }

	double getContainerGroupAggregateReroutePenalty(const PtContainerGroupInterface & cg_ifc) const { return cost_calculator_.calculateCargoReroutePenalty(*cg_ifc.cg_ptr_); }
	double getContainerGroupUnitDelayPenalty(const PtContainerGroupInterface & cg_ifc, TArcIntId port_stay_arc_iid) const;
	double getTransportPlanUnitHandlingCost(const PtTransportPlanInterface & tp_ifc) const { return cost_calculator_.calculateCost(*tp_ifc.tp_, container_group_ifcs_[tp_ifc.getContainerGroupId()].getContainerType()); }

private:

	PtGraphInterface graph_ifc_;

	std::vector<PtVesselInterface> vessel_ifcs_;
	std::vector<PtSiteInterface> site_ifcs_;
	std::vector<PtSailingPlanInterface> sailing_plan_ifcs_;
	std::vector<PtSailingLegInterface> sailing_leg_ifcs_;
	std::vector<PtVesselSiteCallInterface> vessel_site_call_ifcs_;
	std::vector<PtAvailabilityWindowInterface> availability_window_ifcs_;
	std::vector<PtSiteShiftInterface> site_shift_ifcs_;

	std::vector<PtContainerGroupInterface> container_group_ifcs_;
	std::vector<PtTransportPlanInterface> transport_plan_ifcs_;
	std::vector<PtConnectionInterface> connection_ifcs_;


	const CostCalculator & cost_calculator_;

	void printSummary(std::ostream & os) const;
	void printVessels(std::ostream & os) const;
	void printSites(std::ostream & os) const;
	void printVesselSiteCalls(std::ostream & os) const;
	void printSailingPlans(std::ostream & os) const;
	void printSailingLegs(std::ostream & os) const;
	void printContainerGroups(std::ostream & os) const;
	void printTransportPlans(std::ostream & os) const;
	void printTransportPlanLegs(std::ostream & os) const;
	void printConnections(std::ostream & os) const;
	void printAvailabilityWindows(std::ostream & os) const;
	void printSiteShifts(std::ostream & os) const;

	void printGraphSailingArcs(std::ostream & os) const;
	void printGraphPortStayArcs(std::ostream & os) const;
	void printGraphVertices(std::ostream & os) const;
};
