#pragma once

#include "common_types.h"

#include "domain/inc/domain_util_types.h"
#include "domain/inc/vessel.h"
#include "domain/inc/container_group.h"
#include "domain/inc/connecting_time_window.h"
#include "domain/inc/site.h"
#include "rule_engine/inc/rule_engine.h"
#include "controller/inc/scenario_parameters.h"

#include "graph/inc/graph.h"

#include "pt_data_interface.h"



struct TTimePeriodHasher
{
	std::size_t operator()(const TTimePeriod& time_period) const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, time_period.begin().date().day_number());
		boost::hash_combine(seed, time_period.begin().time_of_day().total_nanoseconds());
		boost::hash_combine(seed, time_period.end().date().day_number());
		boost::hash_combine(seed, time_period.end().time_of_day().total_nanoseconds());
		return seed;
	}
};

class PtDataInterfaceBuilder
{
	
public:
	PtDataInterfaceBuilder() = default;

	std::unique_ptr<PtDataInterface> buildDataInterface(
		const SiteRegistry & site_registry,
		const VesselRegistry & vessel_registry,
		const ContainerGroupRegistry & container_group_registry,
		const ConnectingTimeWindowsRegistry & connecting_time_window_registry,
		const ScenarioParameters & scenario_parameters,
		const RuleEngine & rule_engine,
		const Graph & graph
	);

private:

	std::unique_ptr<PtDataInterface> data_interface_;

	// Mappings used while building DataInterface
	std::unordered_map<TSiteCode, TSiteIntId> site_code_to_site_int_id;
	// TODO: Use pair of int IDs instead of string as key; analyse what consumes time in processArcs function
	std::unordered_map < std::string, TVesselSiteCallIntId > vessel_id_site_call_key_pair_to_vessel_site_call_int_id_map;
	std::unordered_map<TIdentifier, TVesselIntId> vessel_id_to_vessel_int_id_map;
	std::unordered_map<std::pair<TVesselSiteCallIntId, TVesselSiteCallIntId>, TLegIntId, boost::hash<std::pair<TVesselSiteCallIntId, TVesselSiteCallIntId>>> vessel_site_call_from_to_pair_to_leg_int_id_map;
	std::unordered_map<std::pair<TVesselSiteCallIntId, TVesselSiteCallIntId>, TConnectionIntId, boost::hash<std::pair<TVesselSiteCallIntId, TVesselSiteCallIntId>>> vessel_site_call_from_to_pair_to_connection_int_id_map;
	std::unordered_map < std::string, TContainerGroupIntId > cg_shipment_num_to_cg_int_id_map;
	std::vector<std::unordered_map<TTimePeriod, TSiteShiftIntId, TTimePeriodHasher>> site_id_shift_time_span_to_site_shift_int_id_map;	// one mapping for each site ID: maps TTimeDuration (shift) to TSiteShiftIntId


	void processSites(const SiteRegistry & site_registry);
	void processVessels(const VesselRegistry & vessel_registry, const Graph & graph);
	void processVesselSailingPlans();
	void processVesselSiteCalls(const ScenarioParameters & scenario_parameters, const Graph& graph);
	void processSailingLegs(const DistanceCalculator & distance_calculator);
	void processContainerGroups(const ContainerGroupRegistry & cg_registry);
	void processTransportPlans();
	void processConnections(const ConnectingTimeWindowsRegistry& connecting_time_window_registry);
	void processAvailabilityWindows();
	void processSiteShifts();
	void processArcs(const Graph& graph);
	void processVertices(const Graph& graph);

	void matchSailingPlansAndTransportPlans();
	void matchVesselSiteCallsAndSiteShifts();
	void matchPortStayArcsAndSiteShifts();
	void identifyControllableSailingLegs();
	void identifyControllableTransportPlans();
	void identifyControllableConnections();
	void identifyInfeasibleTransportPlans();
	void identifyDominatedTransportPlans();
	void identifyAlwaysFeasibleConnections();

	PtVesselInterface & addVesselInterface(const TConstVesselPtr & vsl_ptr);
	PtVesselSiteCallInterface & addVesselSiteCallInterface(const TConstVesselPtr & vsl_ptr, const TSiteCallKey & site_call_key, const ScenarioParameters & scenario_parameters, bool check_if_exists = false);
	PtSiteInterface & addSiteInterface(const Site * site_code, bool check_if_exists = false);
	PtSailingPlanInterface & addSailingPlanInterface(TVesselIntId vessel_id, const TVesselNonDatedSailingPlan & sp);
	PtContainerGroupInterface & addContainerGroupInterface(const TContainerGroupPtr cg_ptr);
	PtTransportPlanInterface & addTransportPlanInterface(TContainerGroupIntId container_group_id, const TContainerGroupTransportPlanConstPtr & tp);
	PtSailingLegInterface & addSailingLegInterface(TVesselIntId vessel_id, TVesselSiteCallIntId origin_vessel_site_call_id, TVesselSiteCallIntId destination_vessel_site_call_id, bool check_if_exists = false);
	PtConnectionInterface & addConnectionInterface(const ConnectingTimeWindowsRegistry& connecting_time_window_registry, TVesselSiteCallIntId vessel_site_call_from_id, TVesselSiteCallIntId vessel_site_call_to_id, bool check_if_exists = false);
	PtAvailabilityWindowInterface & addAvailabilityWindowInterface(TSiteIntId site_id, const SiteAvailabilityWindow & availability_window);
	PtSiteShiftInterface & addSiteShiftInterface(TSiteIntId site_id, const SiteShift & shift, bool check_if_exists = false);

	// We do not have separate interface implementations for each arc, but use the PtGraphInterface to store/handle arc type specific information

	void addSailingArc(TArcIntId arc_iid);
	void addPortStayArc(TArcIntId arc_iid);
	void addTransshipmentArc(TArcIntId arc_iid);
	void addCargoEntryArc(TArcIntId arc_iid);
	void addCargoExitArc(TArcIntId arc_iid);

	bool doesConnectingTimeWindowInterfaceExist(TVesselSiteCallIntId vessel_site_call_from_id, TVesselSiteCallIntId vessel_site_call_to_id) {
		return vessel_site_call_from_to_pair_to_connection_int_id_map.find(std::make_pair(vessel_site_call_from_id, vessel_site_call_to_id))->second; 
	}
};
