#pragma once

#include "common_types.h"
#include "domain/inc/vessel.h"


class PtSailingPlanInterface
{
	friend class PtDataInterfaceBuilder;

public:

	PtSailingPlanInterface(TSailingPlanIntId id, TVesselIntId vsl_id, const TVesselNonDatedSailingPlan & sp)
	: id_(id)
	, vessel_id_(vsl_id)
	, sp_(sp)
	{}

	TSailingPlanIntId getId() const { return id_; }
	TVesselIntId getVesselId() const { return vessel_id_; }
	
	const std::vector<TVesselSiteCallIntId> & getVesselSiteCallIds() const { return vessel_site_call_ids_; }
	const std::vector<TLegIntId> & getSailingLegIds() const { return sailing_leg_ids_; }
	const std::vector<TSiteCallKey> & getSiteCallKeys() const { return sp_; }
	const std::vector<std::pair<TTransportPlanIntId, size_t>> & getCompatibleTransportPlanIdLegNumPairs() const { return compatible_tp_id_leg_num_pairs_; }
	
private:
	
	TSailingPlanIntId id_;
	TVesselIntId vessel_id_;

	const TVesselNonDatedSailingPlan & sp_;

	std::vector<TVesselSiteCallIntId> vessel_site_call_ids_;
	std::vector<TLegIntId> sailing_leg_ids_;
	std::vector<std::pair<TTransportPlanIntId, size_t>> compatible_tp_id_leg_num_pairs_;

};
