#pragma once

#include "common_types.h"
#include "pt_data_interface_defs.h"


class PtAvailabilityWindowInterface
{
	friend class PtDataInterfaceBuilder;

public:
	PtAvailabilityWindowInterface(TAvailabilityWindowIntId id, TSiteIntId site_id, const SiteAvailabilityWindow & availability_window)
		: id_(id)
		, site_id_(site_id)
		, availability_window_(availability_window)
		, is_limited_to_fuel_group_(false)
	{}

	TAvailabilityWindowIntId getId() const { return id_; }
	TSiteIntId getSiteId() const { return site_id_; }
	//const SiteAvailabilityWindow & getSiteAvailabilityWindow() const { return site_availability_window_; }
	const TTimePeriod & getTimeSpan() const { return availability_window_.getWindow(); }
	bool isLimitedToParticularVesselIds() const { return !limited_to_vessel_ids_.empty(); }
	bool isLimitedToFuelGroup() const { return is_limited_to_fuel_group_; }

	const std::vector<TVesselIntId> & getLimitedToVesselIds() const{ return limited_to_vessel_ids_; }
	const std::vector<TSiteShiftIntId> & getSiteShiftIds() const { return shift_ids_; }
	const std::vector<TVesselSiteCallIntId> & getVesselSiteCallIds() const { return vessel_site_call_ids_; }

private:

	TAvailabilityWindowIntId id_;
	TSiteIntId site_id_;
	const SiteAvailabilityWindow & availability_window_;

	std::vector<TVesselIntId> limited_to_vessel_ids_;		// If availability window is limited to particular vessel IDs, this vector is not empty and contains those vessel IDs
	bool is_limited_to_fuel_group_;	// Needs further consideration; currently only status is stored

	std::vector<TSiteShiftIntId> shift_ids_;
	std::vector<TVesselSiteCallIntId> vessel_site_call_ids_;

};
