#pragma once

#include "common_types.h"
#include "pt_data_interface_defs.h"


class PtSiteShiftInterface
{
	friend class PtDataInterfaceBuilder;

public:
	PtSiteShiftInterface(TSiteShiftIntId id, TSiteIntId site_id, const SiteShift& shift)
		: id_(id)
		, site_id_(site_id)
		, shift_(shift)
	{}

	TAvailabilityWindowIntId getId() const { return id_; }
	TSiteIntId getSiteId() const { return site_id_; }
	const std::vector <TAvailabilityWindowIntId> & getAvailabilityWindowIds() const { return availability_window_ids_; }
	const std::vector<TVesselSiteCallIntId> & getVesselSiteCallIds() const { return vessel_site_call_ids_; }
	const TTimePeriod & getTimeSpan() const { return shift_.getShift(); }
	const std::vector<TArcIntId> & getPortStayArcIds() const { return port_stay_arc_ids_; }

private:

	TSiteShiftIntId id_;
	TSiteIntId site_id_;
	const SiteShift& shift_;

	std::vector <TAvailabilityWindowIntId> availability_window_ids_;	// TAvailabilityWindowIntIds of availability windows that contain shift
	std::vector<TVesselSiteCallIntId> vessel_site_call_ids_;	// TVesselSiteCallIntIds of vessel site calls that may use shift
	std::vector<TArcIntId> port_stay_arc_ids_;	// Port stay arcs that would 'consume' shift
};
